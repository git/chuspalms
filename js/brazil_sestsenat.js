/*
@licstart The following is the entire license notice for the JavaScript code
in this page.

ChuspaLMS: Client hybridization used as substitute for the proprietary to
access learning management systems.
Copyright (C) 2021, 2024  Adonay Felipe Nogueira <https://libreplanet.org/wiki/User:Adfeno> <adfeno.7046@gmail.com>

This file is part of ChuspaLMS.

ChuspaLMS is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

ChuspaLMS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with ChuspaLMS.  If not, see <https://www.gnu.org/licenses/>.

As additional permission under GNU GPL version 3 section 7, you may distribute non-source (e.g., minimized or compacted) forms of that code without the copy of the GNU GPL normally required by section 4, provided you include this license notice and a URL through which recipients can access the Corresponding Source.

@licend  The above is the entire license notice for the JavaScript code in this page.
*/

window.onerror = alert;

var unstandardized_anchor_regexp_array = [
  /(?:^|\W+)(?:encerrar[\W_]*inscricao)\(/i,
  /\s*["']?([^"']+)["']?\s*\)/i,
];

var common_request_properties = {
  method: "POST",
}

async function update_address_elements_from_postal_code_input_element(
    municipality_select_element_string,
    select_element_in_same_form_as_event = true,
    event
  ) {
  /*
  * Arguments:
    * municipality_select_element_string: a String or Number that must suffice
      to select only one HTMLSelectElement which will be updated to hold all
      municipalities associated with given federative unit.
    * select_element_in_same_form_as_event: Boolean indicating if the
      municipality select element is the same form as event.target.form.
    * event: Event fired from a HTMLSelectElement holding selected federative
      unit.
  */

  var request_body = JSON.stringify(
    {
      cep: event.target.value
    }
  );

  var request = JSON.parse(JSON.stringify(common_request_properties));
  request.target = document.location.origin +
    "/inicio/auth/cep";
  request.body = request_body;
  request = new Request(request.target,
    request);

  var response = await fetch(request);
  console.debug(
    request,
    request_body,
    response,
    await response.text()
  );
}

if (/^\/checkout\//i.test(document.location.pathname)) {

  old_form = document.forms["checkout"];
  new_form = clone_element_remvoing_event_handlers_and_listeners(
    old_form,
    true
  );

  old_form.parentNode.replaceChild(
    new_form,
    old_form
  );
}
