/*
@licstart The following is the entire license notice for the JavaScript code
in this page.

ChuspaLMS: Client hybridization used as substitute for the proprietary to
access learning management systems.
Copyright (C) 2019-2021, 2024  Adonay Felipe Nogueira <https://libreplanet.org/wiki/User:Adfeno> <adfeno.7046@gmail.com>

This file is part of ChuspaLMS.

ChuspaLMS is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

ChuspaLMS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with ChuspaLMS.  If not, see <https://www.gnu.org/licenses/>.

As additional permission under GNU GPL version 3 section 7, you may distribute non-source (e.g., minimized or compacted) forms of that code without the copy of the GNU GPL normally required by section 4, provided you include this license notice and a URL through which recipients can access the Corresponding Source.

@licend  The above is the entire license notice for the JavaScript code in this page.
*/

window.onerror = alert;

var common_request_properties = {
  method: "POST",
  headers: {
    "Accept": "application/json, text/plain, */*",
    "Content-Type": "application/json; charset=utf-8",
  },
}

async function set_content_as_seen(
    topic_content,
    content_property_names,
    event
  ) {
  /*
  * Arguments:
    * topic_content: any object describing the discipline's material that was
      viewed.
    * content_property_names: pure Object describing common property names for
      each topic content, with each key set to an Array for which:
      * if length of 1 or 2: [0] must be a RegExp to look up against each
        object properties' name; and [1], either undefined or a string
        matching the name found.
      * if length of 3: same as before, but [2] must be another iteration of
        common_property_names.
    * event: an Event fired when opening a HTMLDetailsElement.
  */

  if (! (topic_content instanceof Object)) {
    throw new TypeError(browser.i18n.getMessage(
      "typeerror_instanceof_expected",
      [
        "topic_content",
        "Object",
      ]
    ));
  } else if (! (event.target instanceof HTMLDetailsElement)) {
    throw new TypeError(browser.i18n.getMessage(
      "typeerror_instanceof_expected",
      [
        "event.target",
        "HTMLDetailsElement",
      ]
    ));
  }

  if (event.target.open &&
    topic_content[content_property_names.seen[1]] === false) {

    var request_body = JSON.stringify(
      {
        objConteudo: topic_content,
      }
    );

    var request = JSON.parse(JSON.stringify(common_request_properties));
    request.referrer = document.location.origin +
      document.location.pathname +
      "/visualizar-conteudo";
    request.body = request_body;
    request = new Request(
      request.referrer,
      request
    );

    var response = await fetch(request);

    if (response.status != 200) {
      console.debug(
        request,
        request_body,
        response,
        await response.text()
      );
      throw new Error(browser.i18n.getMessage(
        "error_unhandled_response_status"
      ));
    }
  }
}

async function send_discipline_work(
    topic_content,
    event
  ) {
  /*
  * Arguments:
    * topic_content: any object describing the discipline's material that has the filled work form.
    * event: an Event fired against a HTMLFormElement.
  */

  event.preventDefault();

  if (! (topic_content instanceof Object)) {
    throw new TypeError(browser.i18n.getMessage(
      "typeerror_instanceof_expected",
      [
        "topic_content",
        "Object",
      ]
    ));
  } else if (! (event.target instanceof HTMLFormElement)) {
    throw new TypeError(browser.i18n.getMessage(
      "typeerror_instanceof_expected",
      [
        "event.target",
        "HTMLFormElement",
      ]
    ));
  }

  var work_object = {
    objConteudo: topic_content,
  };

  add_formdata_to_object(
    new FormData(event.target),
    work_object
  );
  var request_body = add_object_to_formdata(work_object);

  var request = JSON.parse(
    JSON.stringify(
      common_request_properties
    )
  );
  delete request.headers;
  request.referrer = document.location.origin +
    document.location.pathname +
    "/salvar-trabalho";
  request.body = request_body;
  request = new Request(
    request.referrer,
    request
  );

  var response = await fetch(request);
  if (response.status == 200) {
    alert(browser.i18n.getMessage("form_submission_succeeded"));
    document.location.reload();
  } else {
    console.debug(
      request,
      [
        ...request_body
      ],
      response,
      await response.text()
    );
    throw new Error(browser.i18n.getMessage(
      "error_unhandled_response_status"
    ));
  }
}

async function send_discipline_test(
    topic_content,
    test_content,
    option_property_names,
    event
  ) {
  /*
  * Arguments:
    * topic_content: any object describing the discipline's material that has
      the test announcement.
    * test_content: any object describing the discipline's test content.
    * option_property_names: pure Object describing common property names for
      the options, with each key set to an Array for which:
      * if length of 1 or 2: [0] must be a RegExp to look up against each
        object properties' name; and [1], either undefined or a string
        matching the name found.
      * if length of 3: same as before, but [2] must be another iteration of
        common_property_names.
    * event: an Event fired against a HTMLFormElement.
  */

  event.preventDefault();

  if (! (topic_content instanceof Object)) {
    throw new TypeError(browser.i18n.getMessage(
      "typeerror_instanceof_expected",
      [
        "topic_content",
        "Object",
      ]
    ));
  } else if (! (test_content instanceof Object)) {
    throw new TypeError(browser.i18n.getMessage(
      "typeerror_instanceof_expected",
      [
        "test_content",
        "Object",
      ]
    ));
  } else if (! (event.target instanceof HTMLFormElement)) {
    throw new TypeError(browser.i18n.getMessage(
      "typeerror_instanceof_expected",
      [
        "event.target",
        "HTMLFormElement",
      ]
    ));
  }

  var input_formdata = new FormData(event.target);

  for (let formdata_entry of input_formdata) {
    if (event.target[formdata_entry[0]] instanceof RadioNodeList) {
      set_object_or_array_property_by_string(
        test_content,
        formdata_entry[1] +
          "["+
          option_property_names.chosen[1] +
          "]",
        true
      );
    } else {
      set_object_or_array_property_by_string(
        test_content,
        formdata_entry[0],
        formdata_entry[1]
      );
    }
  }

  var request_body = JSON.stringify(
    {
      arrConteudo: topic_content,
      arrProva: test_content,
    }
  );

  var request = JSON.parse(JSON.stringify(common_request_properties));
  request.referrer = document.location.origin +
    "/lms/sala/prova/resolucao/" +
    document.location.pathname.split("/").slice(-2).join("/") +
    "/finalizar-prova-json";
  request.body = request_body;
  request = new Request(
    request.referrer,
    request
  );

  var response = await fetch(request);
  if (response.status == 200) {
    alert(browser.i18n.getMessage("form_submission_succeeded"));
    document.location.reload();
  } else {
    console.debug(
      request,
      request_body,
      response,
      await response.text()
    );
    throw new Error(browser.i18n.getMessage(
      "error_unhandled_response_status"
    ));
  }
}

async function prepare_discipline_test(
    topic_content,
    event
  ) {
  /*
  * Arguments:
    * topic_content: any object describing the discipline's material that has
      the test announcement.
    * event: an Event fired against a HTMLFormElement.
  */

  event.preventDefault();

  if (! (topic_content instanceof Object)) {
    throw new TypeError(browser.i18n.getMessage(
      "typeerror_instanceof_expected",
      [
        "topic_content",
        "Object",
      ]
    ));
  } else if (! (event.target instanceof HTMLFormElement)) {
    throw new TypeError(browser.i18n.getMessage(
      "typeerror_instanceof_expected",
      [
        "event.target",
        "HTMLFormElement",
      ]
    ));
  }

  var request_body = JSON.stringify(
    {
      objConteudo: topic_content,
    }
  );

  var request = JSON.parse(JSON.stringify(common_request_properties));
  request.referrer = document.location.origin +
    "/lms/sala/prova/" +
    document.location.pathname.split("/").slice(-2).join("/") +
    "/prova-json";
  request.body = request_body;
  request = new Request(
    request.referrer,
    request
  );

  var response = await fetch(request);
  if (response.status == 200) {
    response = await response.json();

    console.debug(response);
    var test_name = get_object_properties_by_name_regexp(
      response,
      /^arr(?:ay)?[\W_]*prova$/i
    );
    var questions_list_name = get_object_properties_by_name_regexp(
      response[test_name],
      /^arr(?:ay)?[\W_]*questoes$/i
    );
    var question_property_names = {
      description: [
        /^me(?:nsagem)?[\W_]*descricao$/i,
      ],
      type: [
        /arr(?:ay)?[\W_]*questao[\W_]*tipo/i,
        undefined,
        {
          key: [
            /^(?:ds|descricao)[\W_]*chave$/i,
          ],
        },
      ],
      options_list: [
        /^arr(?:ay)?[\W_]*opcoes$/i,
      ],
    };
    var option_property_names = {
      description: [
        /^(?:ds|descricao)[\W_]*opcao$/i,
      ],
      chosen: [
        /^(?:sn|sinal)[\W_]*correta$/i,
      ]
    };

    var initial_mark = document.querySelector("section.chuspalms.material");
    document.querySelectorAll(
      "section.chuspalms.forum, section.chuspalms.chat"
    ).forEach(remove_element);

    var test_form = new Map(
      [
        [
          "root",
          document.createDocumentFragment(),
        ],
        [
          "form",
          new Map(
            [
              [
                "root",
                document.createElement("form"),
              ],
            ],
          ),
        ],
      ]
    );

    test_form.get("form").get("root").className = manifest.name.toLowerCase() +
      " test";

    test_form.get("form").get("root").addEventListener(
      "submit",
      send_discipline_test.bind(
        null,
        topic_content,
        response[test_name],
        option_property_names
      )
    );
    console.log(test_form.get("form").get("root"));

    for (let question_index in response[test_name][questions_list_name]) {
      if (
          ! response[test_name][questions_list_name]
            .hasOwnProperty(question_index)
        ) {

        continue;
      }

      var question = response[test_name][questions_list_name][question_index];

      get_common_property_names_from_member_object(
        question_property_names,
        question
      );

      var question_identity_and_name = questions_list_name +
        "[" +
        question_index +
        "]";
      var question_details_identity = question_identity_and_name +
        "[" +
        question_property_names.description[1] +
        "]";

      test_form.get("form").set(
        question_index,
        new Map(
          [
            [
              "root",
              document.createElement("fieldset"),
            ],
            [
              "title",
              document.createElement("legend"),
            ],
            [
              "details",
              document.createElement("article"),
            ],
            [
              "answer",
              new Map(
                [
                  [
                    "root",
                    document.createElement("div"),
                  ],
                ]
              ),
            ],
          ]
        )
      );

      test_form.get("form").get(question_index).get("root").className =
        manifest.name.toLowerCase();
      test_form.get("form").get(question_index).get("title").innerText =
        new Number(question_index) + 1 +
        " " +
        question[question_property_names.type[1]]
          [question_property_names.type[2].key[1]];
      test_form.get("form").get(question_index).get("details").id =
        question_details_identity;
      test_form.get("form").get(question_index).get("details").innerHTML =
        question[question_property_names.description[1]];
      
      test_form.get("form").get(question_index).get("answer").get("root")
        .setAttribute(
          "aria-details",
          question_details_identity
        );

      if (
          question[question_property_names.type[1]][question_property_names.type[2].key[1]]
             == "OBJETIVA"
        ) {
        test_form.get("form").get(question_index).get("answer").get("root")
          .setAttribute(
            "role",
            "radiogroup"
          );

        for (let option_index in
            question[question_property_names.options_list[1]]
          ) {

          if (
              ! question[question_property_names.options_list[1]]
                .hasOwnProperty(option_index)
            ) {

            continue;
          }

          var option = question[question_property_names.options_list[1]]
            [option_index];

          get_common_property_names_from_member_object(
            option_property_names,
            option
          );

          var option_identity_and_value = question_identity_and_name +
            "["+
            question_property_names.options_list[1] +
            "][" +
            option_index +
            "]";

          var option_label = document.createElement("template");
          option_label.innerHTML =
            option[option_property_names.description[1]];

          test_form.get("form").get(question_index).get("answer").set(
            option_index,
            new Map(
              [
                [
                  "root",
                  document.createElement("p"),
                ],
                [
                  "input",
                  document.createElement("input"),
                ],
                [
                  "label",
                  document.createElement("label"),
                ],
              ]
            ));

          test_form.get("form").get(question_index).get("answer")
            .get(option_index).get("input").name = question_identity_and_name;
          test_form.get("form").get(question_index).get("answer")
            .get(option_index).get("input").type = "radio";
          test_form.get("form").get(question_index).get("answer")
            .get(option_index).get("input").required = true;
          test_form.get("form").get(question_index).get("answer")
            .get(option_index).get("input").id = option_identity_and_value;
          test_form.get("form").get(question_index).get("answer")
            .get(option_index).get("input").value = option_identity_and_value;

          test_form.get("form").get(question_index).get("answer")
            .get(option_index).get("label").setAttribute(
              "for",
              option_identity_and_value
            );
          test_form.get("form").get(question_index).get("answer")
            .get(option_index).get("label").innerHTML =
              option_label.content.textContent;
        }
      }
    }

    test_form.get("form").set(
      "submit_button",
      document.createElement("button")
    );
    test_form.get("form").get("submit_button").type = "submit";
    test_form.get("form").get("submit_button").innerText =
      browser.i18n.getMessage("submit");

    append_map_entries_to_root_node(test_form);
    initial_mark.parentNode.replaceChild(
      test_form.get("root"),
      initial_mark
    );
  } else {
    console.debug(
      convert_request_or_response_to_object(request),
      JSON.parse(request_body),
      convert_request_or_response_to_object(response),
      await response.text());
    throw new Error(browser.i18n.getMessage(
      "error_unhandled_response_status"
    ));
  }
}

async function post_forum_comment(event) {
  // FIXME: Study if preventing default is needed.
  // event.preventDefault();

  var request_body = add_formdata_to_object(new FormData(event.target));

  /*
  headers: {
    "Content-Type": "application/json; charset=utf-8",
  },
  */
  var request = JSON.parse(JSON.stringify(common_request_properties));
  delete request.headers;
  request.referrer = document.location.origin +
    "/lms/forum/mensagem";
  request.body = JSON.stringify(request_body);
  request = new Request(
    request.referrer,
    request
  );

  var response = await fetch(request);
  if (response.status == 200) {
    alert(browser.i18n.getMessage("form_submission_succeeded"));
    document.location.reload(true);
  } else {
    console.debug(
      request,
      [
        ...request_body,
      ],
      response,
      await response.text()
    );
    throw new Error(browser.i18n.getMessage(
      "error_unhandled_response_status"
    ));
  }
}

if (/^\/lms\/v2\/home$/i.test(document.location.pathname)) {
  document.querySelectorAll('#loader-wrapper, #page-content')
    .forEach(remove_element);

  var disciplines = new Map(
    [
      [
        "root",
        document.createDocumentFragment(),
      ],
    ]
  );

  (async function make_disciplines_list() {
    var request = new Request(document.location.origin +
      "/lms/v2/home/listar-meus-cursos-json");
    var response = await fetch(request);

    if (response.status == 200) {
      response = await response.json();

      var courses_list_name = get_object_properties_by_name_regexp(
        response,
        /^arr(?:ay)?[\W_]*lista$/i
      );

      var course_property_names = {
        name: [
          /^(?:ds|descricao)[\W_]*categoria$/i,
        ],
        steps: [
          /^arr(?:ay)?[\W_]*etapas$/i,
        ],
      };

      var discipline_property_names = {
        name: [
          /^(?:ds|descricao)[\W_]*curso$/i,
        ],
        registry: [
          /^(?:cd|codigo)[\W_]*matricula$/i,
        ],
        hash: [
          /^(?:ds|descricao)[\W_]*hash$/i,
        ],
      };

      if (response[courses_list_name].length) {
        get_common_property_names_from_member_object(
          course_property_names,
          response[courses_list_name][0]
        );

        for (let course_index in response[courses_list_name]) {
          if (! response[courses_list_name].hasOwnProperty(course_index)) {
            continue;
          }

          var course = response[courses_list_name][course_index];

          disciplines.set(
            course_index,
            new Map(
              [
                [
                  "root",
                  document.createElement("section"),
                ],
                [
                  "title",
                  document.createElement("h2"),
                ],
              ]
            )
          );

          disciplines.get(course_index).get("title").innerText =
            course[course_property_names.name[1]];

          if (
              Object.getOwnPropertyNames(
                course[course_property_names.steps[1]]
              ).length > 0
            ) {
          
            var disciplines_list_name =
              get_object_properties_by_name_regexp(
                course[course_property_names.steps[1]]
                [
                  Object.getOwnPropertyNames(
                    course[course_property_names.steps[1]]
                  )[0]
                ],
                /^arr(?:ay)?[\W_]*cursos$/i
              );

            for (let step_index in course[course_property_names.steps[1]]) {
              if (
                  ! course[course_property_names.steps[1]]
                    .hasOwnProperty(step_index)
                ) {

                continue;
              }

              var step = course[course_property_names.steps[1]][step_index];

              if (step[disciplines_list_name].length) {
                get_common_property_names_from_member_object(
                  discipline_property_names,
                  step[disciplines_list_name][0]
                );

                if (! disciplines.get(course_index).has("disciplines")) {
                  disciplines.get(course_index).set(
                    "disciplines",
                    new Map(
                      [
                        [
                          "root",
                          document.createElement("ul"),
                        ],
                      ]
                    )
                  );
                }

                for (let discipline_index in step[disciplines_list_name]) {
                  if (
                      ! step[disciplines_list_name]
                        .hasOwnProperty(discipline_index)
                    ) {

                    continue;
                  }

                  var discipline =
                    step[disciplines_list_name][discipline_index];

                  disciplines.get(course_index).get("disciplines")
                      .set(discipline_index,
                        new Map(
                          [
                            [
                              "root",
                              document.createElement("li"),
                            ],
                            [
                              "title",
                              document.createElement("a"),
                            ],
                          ]
                        )
                      );

                  disciplines.get(course_index)
                    .get("disciplines")
                      .get(discipline_index)
                        .get("title").innerText =
                          discipline[discipline_property_names.name[1]];

                  disciplines.get(course_index)
                    .get("disciplines")
                      .get(discipline_index)
                        .get("title").href =
                          "/lms/sala/" +
                           discipline[discipline_property_names.registry[1]] +
                           "/" +
                           discipline[discipline_property_names.hash[1]];
                }
              }
            }
          }
        }
      }
    } else {
      console.debug(
        request,
        response,
        await response.text()
      );
      throw new Error(browser.i18n.getMessage(
        "error_unhandled_response_status"
      ));
    }

    append_map_entries_to_root_node(disciplines);
    console.debug(
      [
        ...disciplines,
      ]
    );
    document.body.appendChild(disciplines.get("root"));
  })();
} else if (/^\/lms\/sala\/\d+\//i.test(document.location.pathname)) {
  document.querySelectorAll(
    '#loader-wrapper, #page-content, [data-ng-show="sn_carregando_curso"]'
  ).forEach(remove_element);

  var initial_mark = document.querySelector("#footer");
  var discipline = new Map(
    [
      [
        "root",
        document.createDocumentFragment(),
      ],
    ]
  );

  for (let section of ["material", "forum", "chat"]) {
    discipline.set(
      section,
      new Map(
        [
          [
            "root",
            document.createElement("section"),
          ],
          [
            "title",
            document.createElement("h2"),
          ],
        ]
      )
    );

    discipline.get(section).get("root").className =
      manifest.name.toLowerCase() +
      " " +
      section;
    discipline.get(section).get("title").innerText =
      browser.i18n.getMessage("discipline_" +
      section);
  }

  (async function make_contents_list() {
    var request = new Request(
      document.location.origin +
      document.location.pathname +
      "/listar-topico-conteudo"
    );
    var response = await fetch(request);

    if (response.status == 200) {
      response = await response.json();
      console.debug(response);

      var topics_list_name = get_object_properties_by_name_regexp(
        response,
        /^result$/i
      );
      var topic_property_names = {
        order: [
          /^(?:nr|numero)[\W_]*ordem$/i,
        ],
        title: [
          /^(?:ds|descricao)[\W_]*titulo$/i,
        ],
        content_list: [
          /^arr(?:ay)?[\W_]*conteudos$/i,
        ],
      };
      var content_property_names = {
        order: [
          /^(?:nr|numero)[\W_]*ordem$/i,
        ],
        title: [
          /^(?:ds|descricao)[\W_]*titulo$/i,
        ],
        seen: [
          /^(?:sn|sinal)[\W_]*visualizado$/i,
        ],
        type: [
          /^arr(?:ay)?[\W_]*conteudo[\W_]*tipo$/i,
          undefined,
          {
            key: [
              /^(?:ds|descricao)[\W_]*chave$/i,
            ],
          },
        ],
        period: [
          /^arr(?:ay)?[\W_]*periodo$/i,
          undefined,
          {
            start_date: [
              /^(?:dt|data)[\W_]*inicio$/i,
            ],
            start_time: [
              /^(?:hr|hora)[\W_]*inicio$/i,
            ],
            end_date: [
              /^(?:dt|data)[\W_]*fim$/i,
            ],
            end_time: [
              /^(?:hr|hora)[\W_]*fim$/i,
            ],
          },
        ],
        message: [
          /^me(?:nsagem)?[\W_]*texto$/i,
        ],
        anchor: [
          /^me(?:nsagem)?[\W_]*link$/i,
        ],
        file: [
          /^arr(?:ay)?[\W_]*conteudo[\W_]*arquivo$/i,
          undefined,
          {
            file: [
              /^arr(?:ay)?[\W_]*arquivo$/i,
              undefined,
              {
                name: [
                  /^(?:ds|descricao)[\W_]*nome$/i,
                ],
                type: [
                  /^(?:ds|descricao)[\W_]*tipo$/i,
                ],
                creation_date: [
                  /^(?:dt|data)[\W_]*criacao$/i,
                ],
                url: [
                  /^(?:ds|descricao)[\W_]*url$/i,
                ],
              },
            ],
          },
        ],
        block_new_answer: [
          /^(?:sn|sinal)[\W_]*bloquear[\W_]*nova[\W_]*resposta$/i,
        ],
        test: [
          /^arr(?:ay)?[\W_]*prova$/i,
          undefined,
          {
            done: [
              /^(?:sn|sinal)[\W_]*respondida[\W_]*aluno$/i,
            ],
            attempts_left: [
              /^(?:nr|numero)[\W_]*tentativa$/i,
            ],
            best_grade: [
              /^(?:vl|valor)[\W_]*melhor[\W_]*nota$/i,
            ],
            weight: [
              /^(?:vl|valor)[\W_]*peso$/i,
            ],
          },
        ],
        enable_descriptive: [
          /^(?:sn|sinal)[\W_]*permitir[\W_]*descritivo$/i,
        ],
        enable_file: [
          /^(?:sn|sinal)[\W_]*enviar[\W_]*arquivo$/i,
        ],
        work: [
          /^arr(?:ay)?[\W_]*trabalho$/i,
          undefined,
          {
            identity: [
              /^(?:cd|codigo)[\W_]*trabalho[\W_]*pessoa$/i,
            ],
            best_grade: [
              /^(?:vl|valor)[\W_]*nota$/i,
            ],
            text: [
              /^me(?:nsagem)?[\W_]*texto$/i,
            ],
            file_identity: [
              /^(?:cd|codigo)[\W_]*arquivo$/i,
            ],
            file_name: [
              /^(?:ds|descricao)[\W_]*nome$/i,
            ],
            file_type: [
              /^(?:ds|descricao)[\W_]*tipo$/i,
            ],
            file_url: [
              /^(?:ds|descricao)[\W_]*url$/i,
            ],
          },
        ],
        work_weight: [
          /^(?:vl|valor)[\W_]*peso$/i,
        ],
        review: [
          /^arr(?:ay)?[\W_]*trabalho[\W_]*pessoa[\W_]*mensagem$/i,
          undefined,
          {
            message: [
              /^me(?:nsagem)?[\W_]*descricao$/i,
            ],
          },
        ],
      };

      if (response[topics_list_name].length) {

        for (let topic_index in response[topics_list_name]) {
          if (! response[topics_list_name].hasOwnProperty(topic_index)) {
            continue;
          }

          var topic = response[topics_list_name][topic_index];

          get_common_property_names_from_member_object(
            topic_property_names,
            topic
          );

          discipline.get("material").set(
            topic_index,
            new Map(
              [
                [
                  "root",
                  document.createElement("section"),
                ],
                [
                  "title",
                  document.createElement("h3"),
                ],
              ]
            )
          );

          discipline.get("material").get(topic_index).get("title").innerText =
            topic[topic_property_names.order[1]] +
            " " +
            topic[topic_property_names.title[1]];

          if (topic[topic_property_names.content_list[1]].length) {
            for (let content_index in
              topic[topic_property_names.content_list[1]]) {

              if (
                  ! topic[topic_property_names.content_list[1]]
                    .hasOwnProperty(content_index)
                ) {

                continue;
              }

              var content = topic[topic_property_names.content_list[1]]
                [content_index];

              get_common_property_names_from_member_object(
                content_property_names,
                content
              );

              discipline.get("material").get(topic_index).set(
                content_index,
                new Map(
                  [
                    [
                      "root",
                      document.createElement("details"),
                    ],
                    [
                      "summary",
                      new Map(
                        [
                          [
                            "root",
                            document.createElement("summary"),
                          ],
                          [
                            "title",
                            document.createElement("h4"),
                          ],
                          [
                            "seen",
                            new Map(
                              [
                                [
                                  "root",
                                  document.createElement("label"),
                                ],
                                [
                                  "text",
                                  undefined
                                ],
                                [
                                  "status",
                                  document.createElement("input"),
                                ],
                              ]
                            ),
                          ],
                        ]
                      ),
                    ],
                  ]
                )
              );

              discipline.get("material").get(topic_index).get(content_index)
                .get("root").className = manifest.name.toLowerCase();

              discipline.get("material").get(topic_index).get(content_index)
                .get("root").addEventListener(
                  "toggle",
                  set_content_as_seen.bind(
                    null,
                    content,
                    content_property_names
                  )
                );

              discipline.get("material").get(topic_index).get(content_index)
                .get("summary").get("title").innerText =
                  content[content_property_names.order[1]] +
                  " " +
                  (content[content_property_names.title[1]] ?
                    content[content_property_names.title[1]] :
                    content[content_property_names.type[1]]
                      [content_property_names.type[2].key[1]]
                  );

              discipline.get("material").get(topic_index).get(content_index)
                .get("summary").get("seen").set(
                  "text",
                  document.createTextNode(browser.i18n.getMessage("seen"))
                );

              discipline.get("material").get(topic_index).get(content_index)
                .get("summary").get("seen").get("status").type = "checkbox";
              discipline.get("material").get(topic_index).get(content_index)
                .get("summary").get("seen").get("status").disabled = true;
              discipline.get("material").get(topic_index).get(content_index)
                .get("summary").get("seen").get("status").checked =
                  content[content_property_names.seen[1]];

              discipline.get("material").get(topic_index).get(content_index)
                .get("summary").set(
                  "type",
                  document.createElement("span")
                );
              discipline.get("material").get(topic_index).get(content_index)
                .get("summary").get("type").innerText =
                  content[content_property_names.type[1]]
                    [content_property_names.type[2].key[1]];

              if (
                  content[content_property_names.period[1]]
                    [content_property_names.period[2].start_date[1]] ||
                  content[content_property_names.period[1]]
                    [content_property_names.period[2].end_date[1]]
                ) {

                delete planned_date_start;
                delete planned_date_end;
                var planned_date_start =
                  convert_date_and_time_string_to_date_object(
                    content[content_property_names.period[1]]
                      [content_property_names.period[2].start_date[1]],
                    (
                      content[content_property_names.period[1]]
                        [content_property_names.period[2].start_time[1]] ||
                      undefined
                    )
                  );
                var planned_date_end =
                  convert_date_and_time_string_to_date_object(
                    content[content_property_names.period[1]]
                      [content_property_names.period[2].end_date[1]],
                    (
                      content[content_property_names.period[1]]
                        [content_property_names.period[2].end_time[1]] ||
                      undefined
                    )
                  );

                if (! isNaN(planned_date_start)) {
                  discipline.get("material").get(topic_index)
                    .get(content_index).get("summary").set(
                      "planned_date_start",
                      document.createElement("time")
                    );
                  discipline.get("material").get(topic_index)
                    .get(content_index).get("summary")
                      .get("planned_date_start").innerText =
                        (
                            ! content[content_property_names.period[1]]
                              [content_property_names.period[2]
                                .start_time[1]] &&
                            isNaN(planned_date_end) ?
                          planned_date_start.toLocaleString().split(" ")[0] :
                          planned_date_start.toLocaleString()
                        );
                  discipline.get("material").get(topic_index)
                    .get(content_index).get("summary")
                      .get("planned_date_start").dateTime =
                        (
                            ! content[content_property_names.period[1]]
                              [content_property_names.period[2]
                                .start_time[1]] &&
                            isNaN(planned_date_end) ?
                          planned_date_start.toISOString().split("T")[0] :
                          planned_date_start.toISOString()
                        );

                  if (! isNaN(planned_date_end)) {
                    discipline.get("material").get(topic_index)
                      .get(content_index).get("summary").set(
                        "planned_date_end",
                        document.createElement("time")
                      );
                    discipline.get("material").get(topic_index)
                      .get(content_index).get("summary")
                        .get("planned_date_end").innerText =
                          planned_date_end.toLocaleString();
                    discipline.get("material").get(topic_index)
                      .get(content_index).get("summary")
                        .get("planned_date_end").dateTime =
                          planned_date_end.toISOString();
                  }
                }
              }

              if (
                  content[content_property_names.type[1]]
                    [content_property_names.type[2].key[1]] == "PROVA" ||
                  (
                    content[content_property_names.type[1]]
                      [content_property_names.type[2].key[1]] == "TRABALHO" &&
                    (
                      content[content_property_names.enable_descriptive[1]] ||
                      content[content_property_names.enable_file[1]]
                    )
                  )
                ) {

                discipline.get("material").get(topic_index)
                  .get(content_index).get("summary").set(
                    "attempts_left",
                    document.createElement("span")
                  );
                discipline.get("material").get(topic_index)
                  .get(content_index).get("summary").set(
                    "done",
                    new Map(
                      [
                        [
                          "root",
                          document.createElement("label"),
                        ],
                        [
                          "text",
                          undefined,
                        ],
                        [
                          "status",
                          document.createElement("input"),
                        ],
                      ]
                    )
                  );
                discipline.get("material").get(topic_index)
                  .get(content_index).get("summary").set(
                    "grade",
                    document.createElement("span")
                  );

                discipline.get("material").get(topic_index)
                  .get(content_index).get("summary").get("done").set(
                    "text",
                    document.createTextNode(browser.i18n.getMessage("done"))
                  );

                discipline.get("material").get(topic_index)
                  .get(content_index).get("summary").get("done")
                    .get("status").type = "checkbox";
                discipline.get("material").get(topic_index)
                  .get(content_index).get("summary").get("done")
                    .get("status").disabled = true;

                if (
                    content[content_property_names.test[1]] &&
                    typeof(content[content_property_names.test[1]]
                      [content_property_names.test[2].attempts_left[1]])
                        == "number"
                  ) {
                  
                  discipline.get("material").get(topic_index)
                    .get(content_index).get("summary")
                      .get("attempts_left").innerText =
                        browser.i18n.getMessage(
                          "attempts_left",
                          [
                            Math.min(
                              Number(
                                ! content[content_property_names
                                  .block_new_answer[1]]
                              ),
                              content[content_property_names.test[1]]
                                [content_property_names.test[2]
                                  .attempts_left[1]]
                            ),
                          ]
                        );
                } else {
                  discipline.get("material").get(topic_index)
                    .get(content_index).get("summary")
                      .get("attempts_left").innerText =
                        browser.i18n.getMessage(
                          "attempts_left",
                          [
                            Number(
                              ! content[content_property_names
                                .block_new_answer[1]]
                            ),
                          ]
                        );
                }

                if (
                    content[content_property_names.type[1]]
                      [content_property_names.type[2].key[1]] == "PROVA"
                  ) {
                  discipline.get("material").get(topic_index)
                    .get(content_index).get("summary")
                      .get("grade").innerText =
                        browser.i18n.getMessage(
                          "grade",
                          [
                            (
                                typeof(content[content_property_names.test[1]]
                                  [content_property_names.test[2]
                                    .best_grade[1]]) == "number" ?
                              content[content_property_names.test[1]]
                                [content_property_names.test[2]
                                  .best_grade[1]] :
                              "?"
                            ),
                            (
                                typeof(content[content_property_names.test[1]]
                                  [content_property_names.test[2]
                                    .weight[1]]) == "number" ?
                              content[content_property_names.test[1]]
                                [content_property_names.test[2]
                                  .weight[1]] :
                              "?"
                            ),
                          ]
                        );

                  discipline.get("material").get(topic_index)
                    .get(content_index).get("summary")
                      .get("done").get("status").checked =
                        content[content_property_names.test[1]]
                          [content_property_names.test[2].done[1]];
                } else if (
                    content[content_property_names.type[1]]
                      [content_property_names.type[2].key[1]] == "TRABALHO"
                  ) {
                  discipline.get("material").get(topic_index)
                    .get(content_index).get("summary")
                      .get("grade").innerText =
                        browser.i18n.getMessage(
                          "grade",
                          [
                            (
                                typeof(content[content_property_names.work[1]]
                                  [content_property_names.work[2]
                                    .best_grade[1]]) == "number" ?
                              content[content_property_names.work[1]]
                                [content_property_names.work[2]
                                  .best_grade[1]] :
                              "?"
                            ),
                            (
                                typeof(content[content_property_names
                                  .work_weight[1]]) == "number" ?
                              content[content_property_names.work_weight[1]] :
                              "?"
                            ),
                          ]
                        );

                  if (
                      content_property_names.work[2].identity[1] &&
                      content[content_property_names.work[1]]
                        [content_property_names.work[2].identity[1]]
                    ) {

                    discipline.get("material").get(topic_index)
                      .get(content_index).get("summary").get("done")
                        .get("status").checked = true;
                  }
                }
              }

              discipline.get("material").get(topic_index)
                .get(content_index).set(
                  "message",
                  document.createElement("p")
                );
              discipline.get("material").get(topic_index)
                .get(content_index).get("message").innerHTML =
                  (
                    content[content_property_names.message[1]] ||
                    browser.i18n.getMessage("no_content")
                  );

              if (
                  content[content_property_names.type[1]]
                    [content_property_names.type[2].key[1]] == "LINK" ||
                  content[content_property_names.type[1]]
                    [content_property_names.type[2].key[1]] == "VIDEO"
                ) {
                discipline.get("material").get(topic_index)
                  .get(content_index).set(
                    "actions",
                    new Map(
                      [
                        [
                          "root",
                          document.createElement("p"),
                        ],
                      ]
                    )
                  );

                var anchor_location = (
                    /^[^\s\d]+[\w+-.]*:(?:\/\/)?/
                      .test(content[content_property_names.anchor[1]]) ||
                    content[content_property_names.anchor[1]] == "" ?

                    "" :
                    document.location.protocol +
                      "//"
                  ) +
                  content[content_property_names.anchor[1]];

                discipline.get("material").get(topic_index)
                  .get(content_index).get("actions").set(
                    "anchor",
                    document.createElement("a")
                  );

                discipline.get("material").get(topic_index)
                  .get(content_index).get("actions").get("anchor").href =
                    anchor_location;
                discipline.get("material").get(topic_index)
                  .get(content_index).get("actions").get("anchor").innerText =
                    anchor_location;
              } else if (
                  content[content_property_names.type[1]]
                    [content_property_names.type[2].key[1]] == "ARQUIVO" ||
                  (
                    content[content_property_names.type[1]
                      ][content_property_names.type[2].key[1]] == "TRABALHO" &&
                    content[content_property_names.file[1]]
                  )
                ) {

                discipline.get("material").get(topic_index)
                  .get(content_index).set(
                    "creation_date",
                    document.createElement("p")
                  );
                discipline.get("material").get(topic_index)
                  .get(content_index).get("creation_date").innerText =
                    content[content_property_names.file[1]]
                      [content_property_names.file[2].file[1]]
                        [content_property_names.file[2]
                          .file[2].creation_date[1]];

                discipline.get("material").get(topic_index)
                  .get(content_index).set(
                    "actions",
                    new Map(
                      [
                        [
                          "root",
                          document.createElement("p"),
                        ],
                      ]
                    )
                  );

                discipline.get("material").get(topic_index)
                  .get(content_index).get("actions").set(
                    "anchor",
                    document.createElement("a")
                  );

                discipline.get("material").get(topic_index)
                  .get(content_index).get("actions").get("anchor").href =
                    content[content_property_names.file[1]]
                      [content_property_names.file[2].file[1]]
                        [content_property_names.file[2].file[2].url[1]];
                discipline.get("material").get(topic_index)
                  .get(content_index).get("actions").get("anchor").download =
                    content[content_property_names.file[1]]
                      [content_property_names.file[2].file[1]]
                        [content_property_names.file[2].file[2].name[1]];
                discipline.get("material").get(topic_index)
                  .get(content_index).get("actions").get("anchor").type =
                    content[content_property_names.file[1]]
                      [content_property_names.file[2].file[1]]
                        [content_property_names.file[2].file[2].type[1]];
                discipline.get("material").get(topic_index)
                  .get(content_index).get("actions").get("anchor").innerText =
                    browser.i18n.getMessage("download");
              }

              if (
                  content[content_property_names.type[1]]
                    [content_property_names.type[2].key[1]] == "TRABALHO"
                ) {

                if (
                    content[content_property_names.review[1]]
                      [content_property_names.review[2].message[1]]
                  ) {
                  discipline.get("material").get(topic_index)
                    .get(content_index).set(
                      "review",
                      new Map(
                        [
                          [
                            "root",
                            document.createElement("div"),
                          ],
                          [
                            "title",
                            document.createElement("h4"),
                          ],
                          [
                            "body",
                            document.createElement("article"),
                          ],
                        ]
                      )
                    );

                  discipline.get("material").get(topic_index)
                    .get(content_index).get("review").get("title").innerText =
                      browser.i18n.getMessage("reviewers_message");

                  discipline.get("material").get(topic_index)
                    .get(content_index).get("review").get("body").innerHTML =
                      content[content_property_names.review[1]]
                        [content_property_names.review[2].message[1]];
                }
                if (
                    content[content_property_names.enable_descriptive[1]] &&
                    content[content_property_names.work[1]] &&
                    content[content_property_names.work[1]]
                      [content_property_names.work[2].text[1]]
                  ) {

                  discipline.get("material").get(topic_index)
                    .get(content_index).set(
                      "last_text_sent",
                      new Map(
                        [
                          [
                            "root",
                            document.createElement("div"),
                          ],
                          [
                            "title",
                            document.createElement("h4"),
                          ],
                          [
                            "body",
                            document.createElement("article"),
                          ],
                        ]
                      )
                    );

                  discipline.get("material").get(topic_index)
                    .get(content_index).get("last_text_sent")
                      .get("title").innerText =
                        browser.i18n.getMessage("last_text_sent");

                  discipline.get("material").get(topic_index)
                    .get(content_index).get("last_text_sent")
                      .get("body").innerHTML =
                        content[content_property_names.work[1]]
                          [content_property_names.work[2].text[1]];
                }

                if (
                    content[content_property_names.enable_file[1]] &&
                    content[content_property_names.work[1]] &&
                    content_property_names.work[2].file_identity[1] &&
                    content[content_property_names.work[1]]
                      [content_property_names.work[2].file_identity[1]]
                  ) {

                  discipline.get("material").get(topic_index)
                    .get(content_index).set(
                      "last_file_sent",
                      new Map(
                        [
                          [
                            "root",
                            document.createElement("div"),
                          ],
                          [
                            "title",
                            document.createElement("h4"),
                          ],
                          [
                            "information",
                            document.createElement("p"),
                          ],
                          [
                            "download",
                            new Map(
                              [
                                [
                                  "root",
                                  document.createElement("p"),
                                ],
                                [
                                  "anchor",
                                  document.createElement("a"),
                                ],
                              ]
                            ),
                          ],
                        ]
                      )
                    );

                  discipline.get("material").get(topic_index)
                    .get(content_index).get("last_file_sent")
                      .get("title").innerText =
                        browser.i18n.getMessage("last_file_sent");

                  discipline.get("material").get(topic_index)
                    .get(content_index).get("last_file_sent")
                      .get("information").innerText =
                        content[content_property_names.work[1]]
                          [content_property_names.work[2].file_name[1]];

                  discipline.get("material").get(topic_index)
                    .get(content_index).get("last_file_sent")
                      .get("download").get("anchor").href =
                        content[content_property_names.work[1]]
                          [content_property_names.work[2].file_url[1]];
                  discipline.get("material").get(topic_index)
                    .get(content_index).get("last_file_sent")
                      .get("download").get("anchor").download =
                        content[content_property_names.work[1]]
                          [content_property_names.work[2].file_name[1]];
                  discipline.get("material").get(topic_index)
                    .get(content_index).get("last_file_sent")
                      .get("download").get("anchor").type =
                        content[content_property_names.work[1]]
                          [content_property_names.work[2].file_type[1]];
                  discipline.get("material").get(topic_index)
                    .get(content_index).get("last_file_sent")
                      .get("download").get("anchor").innerText =
                        browser.i18n.getMessage("download");
                }

                if (
                    (
                      content[content_property_names.test[1]] &&
                      typeof(content[content_property_names.test[1]]
                        [content_property_names.test[2].attempts_left[1]])
                          == "number" &&
                      content[content_property_names.test[1]]
                        [content_property_names.test[2].attempts_left[1]] > 0
                    ) ||
                    ! content[content_property_names.block_new_answer[1]]
                  ) {

                  discipline.get("material").get(topic_index)
                    .get(content_index).set(
                      "form",
                      new Map(
                        [
                          [
                            "root",
                            document.createElement("form"),
                          ],
                          [
                            "title",
                            document.createElement("h4"),
                          ],
                        ]
                      )
                    );

                  discipline.get("material").get(topic_index)
                    .get(content_index).get("form").get("root")
                      .addEventListener(
                        "submit",
                        send_discipline_work.bind(
                          null,
                          content
                        )
                      );

                  discipline.get("material").get(topic_index)
                    .get(content_index).get("form").get("title").innerText =
                      browser.i18n.getMessage("send_work");

                  if (content[content_property_names.enable_descriptive[1]]) {
                    discipline.get("material").get(topic_index)
                      .get(content_index).get("form").set(
                        "text_input",
                        document.createElement("textarea")
                      );
                    discipline.get("material").get(topic_index)
                      .get(content_index).get("form").get("text_input").name =
                        "objConteudo[arrTrabalho][me_texto]";

                    discipline.get("material").get(topic_index)
                      .get(content_index).get("form").set(
                        "preview_input_button",
                        document.createElement("input")
                      );
                    discipline.get("material").get(topic_index)
                      .get(content_index).get("form")
                        .get("preview_input_button").type = "button";

                    discipline.get("material").get(topic_index)
                      .get(content_index).get("form").set(
                        "text_preview",
                        document.createElement("article"));
                    discipline.get("material").get(topic_index)
                      .get(content_index).get("form").get("text_preview").id =
                        "text_preview_" + content_index;

                    discipline.get("material").get(topic_index)
                      .get(content_index).get("form")
                        .get("preview_input_button").addEventListener(
                          "click",
                          preview_textarea_in_article.bind(
                            null,
                            discipline.get("material").get(topic_index)
                              .get(content_index).get("form")
                                .get("text_input"),
                            discipline.get("material").get(topic_index)
                             .get(content_index).get("form")
                               .get("text_preview")
                          )
                        );
                  }

                  if (content[content_property_names.enable_file[1]]) {
                    discipline.get("material").get(topic_index)
                      .get(content_index).get("form").set(
                        "file_input",
                        document.createElement("input")
                      );
                    discipline.get("material").get(topic_index)
                      .get(content_index).get("form").get("file_input").type =
                        "file";
                    discipline.get("material").get(topic_index)
                      .get(content_index).get("form").get("file_input").name =
                        "ds_arquivo";
                  }

                  discipline.get("material").get(topic_index)
                    .get(content_index).get("form").set(
                      "submit_button",
                      document.createElement("button")
                    );
                  discipline.get("material").get(topic_index)
                    .get(content_index).get("form")
                      .get("submit_button").name = "btnSalvarResposta";
                  discipline.get("material").get(topic_index)
                    .get(content_index).get("form")
                      .get("submit_button").innerText =
                        browser.i18n.getMessage("submit");
                  discipline.get("material").get(topic_index)
                    .get(content_index).get("form")
                      .get("submit_button").type = "submit";
                }
              } else if (
                  content[content_property_names.type[1]]
                    [content_property_names.type[2].key[1]] == "PROVA"
                ) {

                discipline.get("material").get(topic_index)
                  .get(content_index).set(
                    "form",
                    new Map(
                      [
                        [
                          "root",
                          document.createElement("form"),
                        ],
                        [
                          "submit_button",
                          document.createElement("button"),
                        ],
                      ]
                    )
                  );

                discipline.get("material").get(topic_index)
                  .get(content_index).get("form")
                    .get("root").addEventListener(
                      "submit",
                      prepare_discipline_test.bind(
                        null,
                        content
                      )
                    );

                discipline.get("material").get(topic_index)
                  .get(content_index).get("form").get("submit_button").type =
                    "submit";
                discipline.get("material").get(topic_index)
                  .get(content_index).get("form")
                    .get("submit_button").innerText =
                      browser.i18n.getMessage("start_test");
              }
            }
          } else {
            discipline.get("material").get(topic_index).set(
              "no_content",
              document.createElement("p")
            );
            discipline.get("material").get(topic_index)
              .get("no_content").innerText =
                browser.i18n.getMessage("no_content");
          }
        }
      } else {
        discipline.get("material").set(
          "no_content",
          document.createElement("p")
        );
        discipline.get("material").get("no_content").innerText =
          browser.i18n.getMessage("no_content");
      }
    } else {
      console.debug(
        request,
        response,
        await response.text()
      );
      throw new Error(browser.i18n.getMessage(
        "error_unhandled_response_status"
      ));
    }

    request = new Request(
      document.location.origin +
      document.location.pathname +
      "/listar-forum"
    );
    response = await fetch(request);

    if (response.status == 200) {
      response = await response.json();

      var topics_list_name = get_object_properties_by_name_regexp(
        response,
        /result/i
      );
      var topic_property_names = {
        identity: [
          /(?:cd|codigo)[\W_]*topico/i,
        ],
        title: [
          /titulo/i,
        ],
        description: [
          /descricao/i,
        ],
        start_date: [
          /(?:dt|data)[\W_]*inicio/i,
        ],
        start_time: [
          /(?:hr|hora)[\W_]*inicio/i,
        ],
        end_date: [
          /(?:dt|data)[\W_]*fim/i,
        ],
        end_time: [
          /(?:hr|hora)[\W_]*fim/i,
        ],
      };

      if (response[topics_list_name].length) {
        for (let topic_index in response[topics_list_name]) {
          if (! response[topics_list_name].hasOwnProperty(topic_index)) {
            continue;
          }

          var topic = response[topics_list_name][topic_index];

          get_common_property_names_from_member_object(
            topic_property_names,
            topic
          );

          discipline.get("forum").set(
            topic_index,
            new Map(
              [
                [
                  "root",
                  document.createElement("details"),
                ],
                [
                  "summary",
                  new Map(
                    [
                      [
                        "root",
                        document.createElement("summary"),
                      ],
                      [
                        "title",
                        new Map(
                          [
                            [
                              "root",
                              document.createElement("h3"),
                            ],
                            [
                              "anchor",
                              document.createElement("a"),
                            ],
                          ]
                        ),
                      ],
                    ]
                  ),
                ],
              ]
            )
          );

          discipline.get("forum").get(topic_index).get("root").className =
            manifest.name.toLowerCase();

          discipline.get("forum").get(topic_index).get("summary")
            .get("title").get("anchor").innerText =
              topic[topic_property_names.title[1]];
          discipline.get("forum").get(topic_index).get("summary")
            .get("title").get("anchor").href = "/lms/forum/" +
              topic[topic_property_names.identity[1]];

          if (
              topic[topic_property_names.start_date[1]] ||
              topic[topic_property_names.end_date[1]]
            ) {

            delete planned_date_start;
            delete planned_date_end;
            var planned_date_start =
              convert_date_and_time_string_to_date_object(
                topic[topic_property_names.start_date[1]],
                (
                  topic[topic_property_names.start_time[1]] ||
                  undefined
                )
              );
            var planned_date_end =
             convert_date_and_time_string_to_date_object(
               topic[topic_property_names.end_date[1]],
               (
                 topic[topic_property_names.end_time[1]] ||
                 undefined
               )
             );

            if (! isNaN(planned_date_start)) {
              discipline.get("forum").get(topic_index).get("summary").set(
                "planned_date_start",
                document.createElement("time")
              );
              discipline.get("forum").get(topic_index).get("summary")
                .get("planned_date_start").innerText =
                  (
                      ! topic[topic_property_names.start_time[1]] &&
                      isNaN(planned_date_end) ?
                    planned_date_start.toLocaleString().split(" ")[0] :
                    planned_date_start.toLocaleString()
                  );
              discipline.get("forum").get(topic_index).get("summary")
                .get("planned_date_start").dateTime =
                  (
                      ! topic[topic_property_names.start_time[1]] &&
                      isNaN(planned_date_end) ?
                    planned_date_start.toISOString().split("T")[0] :
                    planned_date_start.toISOString()
                  );

              if (! isNaN(planned_date_end)) {
                discipline.get("forum").get(topic_index).get("summary").set(
                  "planned_date_end",
                  document.createElement("time")
                );
                discipline.get("forum").get(topic_index).get("summary")
                  .get("planned_date_end").innerText =
                    planned_date_end.toLocaleString();
                discipline.get("forum").get(topic_index).get("summary")
                  .get("planned_date_end").dateTime =
                    planned_date_end.toISOString();
              }
            }
          }

          if (topic[topic_property_names.description[1]]) {
            discipline.get("forum").get(topic_index).set(
              "message",
              document.createElement("p")
            );
            discipline.get("forum").get(topic_index)
              .get("message").innerHTML =
                topic[topic_property_names.description[1]];
          }
        }
      } else {
        discipline.get("forum").set(
          "no_content",
          document.createElement("p")
        );
        discipline.get("forum").get("no_content").innerText =
          browser.i18n.getMessage("no_content");
      }
    } else {
      console.debug(
        request,
        response,
        await response.text()
      );
      throw new Error(browser.i18n.getMessage(
        "error_unhandled_response_status"
      ));
    }

    request = new Request(
      document.location.origin +
      document.location.pathname +
      "/listar-chat-sala"
    );
    response = await fetch(request);

    if (response.status == 200) {
      response = await response.json();

      var topics_list_name = get_object_properties_by_name_regexp(
        response,
        /result/i
      );
      var topic_property_names = {
        name: [
          /nome[\W_]*sala/i,
        ],
        key: [
          /chave/i,
        ],
        start_date: [
          /(?:dt|data)[\W_]*inicio/i,
        ],
        start_time: [
          /(?:hr|hora)[\W_]*inicio/i,
        ],
        end_date: [
          /(?:dt|data)[\W_]*fim/i,
        ],
        end_time: [
          /(?:hr|hora)[\W_]*fim/i,
        ],
      };

      if (response[topics_list_name].length) {
        for (let topic_index in response[topics_list_name]) {
          if (! response[topics_list_name].hasOwnProperty(topic_index)) {
            continue;
          }

          var topic = response[topics_list_name][topic_index];

          get_common_property_names_from_member_object(
            topic_property_names,
            topic
          );

          discipline.get("chat").set(
            topic_index,
            new Map(
              [
                [
                  "root",
                  document.createElement("article"),
                ],
                [
                  "title",
                  new Map(
                    [
                      [
                        "root",
                        document.createElement("h3"),
                      ],
                      [
                        "anchor",
                        document.createElement("a"),
                      ],
                    ]
                  ),
                ],
              ]
            )
          );

          discipline.get("chat").get(topic_index).get("root").className =
            manifest.name.toLowerCase();

          discipline.get("chat").get(topic_index).get("title")
            .get("anchor").innerText = topic[topic_property_names.name[1]];
          discipline.get("chat").get(topic_index).get("title")
            .get("anchor").href = "/chat/aluno/" +
              topic[topic_property_names.key[1]];

          if (
              topic[topic_property_names.start_date[1]] ||
              topic[topic_property_names.end_date[1]]
            ) {

            delete planned_date_start;
            delete planned_date_end;

            var planned_date_start =
              convert_date_and_time_string_to_date_object(
                topic[topic_property_names.start_date[1]],
                (
                  topic[topic_property_names.start_time[1]] ||
                  undefined
                )
              );
            var planned_date_end = convert_date_and_time_string_to_date_object(
              topic[topic_property_names.end_date[1]],
              (
                topic[topic_property_names.end_time[1]] ||
                undefined
              )
            );

            if (! isNaN(planned_date_start)) {
              discipline.get("chat").get(topic_index).set(
                "planned_date_start",
                document.createElement("time")
              );
              discipline.get("chat").get(topic_index)
                .get("planned_date_start").innerText =
                  (
                      ! topic[topic_property_names.start_time[1]] &&
                      isNaN(planned_date_end) ?
                    planned_date_start.toLocaleString().split(" ")[0] :
                    planned_date_start.toLocaleString()
                  );

              discipline.get("chat").get(topic_index)
                .get("planned_date_start").dateTime =
                  (
                      ! topic[topic_property_names.start_time[1]] &&
                      isNaN(planned_date_end) ?
                    planned_date_start.toISOString().split("T")[0] :
                    planned_date_start.toISOString()
                  );

              if (! isNaN(planned_date_end)) {
                discipline.get("chat").get(topic_index).set(
                  "planned_date_end",
                  document.createElement("time")
                );
                discipline.get("chat").get(topic_index)
                  .get("planned_date_end").innerText =
                    planned_date_end.toLocaleString();
                discipline.get("chat").get(topic_index)
                  .get("planned_date_end").dateTime =
                    planned_date_end.toISOString();
              }
            }
          }
        }
      } else {
        discipline.get("chat").set(
          "no_content",
          document.createElement("p")
        );
        discipline.get("chat").get("no_content").innerText =
          browser.i18n.getMessage("no_content");
      }
    } else {
      console.debug(
        request,
        response,
        await response.text()
      );
      throw new Error(browser.i18n.getMessage(
        "error_unhandled_response_status"
      ));
    }

    append_map_entries_to_root_node(discipline);
    initial_mark.parentNode.insertBefore(
      discipline.get("root"),
      initial_mark
    );
  })();
} else if (/^\/lms\/forum\//i.test(document.location.pathname)) {
  var initial_mark = document.querySelector("#me_responder").parentNode;

  document.querySelectorAll(
    "#loader-wrapper, #me_responder, .panel-footer"
  ).forEach(remove_element);

  var comment_form = new Map(
    [
      [
        "root",
        document.createDocumentFragment(),
      ],
      [
        "form",
        new Map(
          [
            [
              "root",
              document.createElement("form"),
            ],
            [
              "topic_code_hidden_input",
              document.createElement("input"),
            ],
            [
              "parent_message_code_hidden_input",
              document.createElement("input"),
            ],
            [
              "comment_textarea",
              document.createElement("textarea"),
            ],
            [
              "parent_message_text_hidden_input",
              document.createElement("input"),
            ],
            [
              "submit_button",
              document.createElement("button"),
            ],
          ]
        ),
      ],
    ]
  );

  comment_form.get("root").get("form").get("root").name = "objMensagem";
  comment_form.get("root").get("form").get("root").addEventListener(
    "submit",
    post_forum_comment
  );

  comment_form.get("root").get("form").get("topic_code_hidden_input").name =
    "cd_topico";
  comment_form.get("root").get("form").get("topic_code_hidden_input").type =
    "hidden";
  comment_form.get("root").get("form").get("topic_code_hidden_input").value =
    document.location.pathname.split("/").slice(-1).join("/");

  comment_form.get("root").get("form")
    .get("parent_message_code_hidden_input").name = "cd_mensagem_citada";
  comment_form.get("root").get("form")
    .get("parent_message_code_hidden_input").type = "hidden";
  // FIXME: Support replying to comments.
  comment_form.get("root").get("form")
    .get("parent_message_code_hidden_input").value = null;

  comment_form.get("root").get("form").get("comment_textarea").name =
    "me_mensagem";
  comment_form.get("root").get("form").get("comment_textarea").minlength =
    document.querySelector("#nr_min_caracter").value;

  comment_form.get("root").get("form")
    .get("parent_message_text_hidden_input").name = "me_mensagem_citadao";
  comment_form.get("root").get("form")
    .get("parent_message_text_hidden_input").type = "hidden";
  // FIXME: Support replying to comments.
  comment_form.get("root").get("form")
    .get("parent_message_text_hidden_input").value = null;

  comment_form.get("root").get("form").get("submit_button").type = "button";
  comment_form.get("root").get("form").get("submit_button").innerText =
    browser.i18n.getMessage("submit");

  append_map_entries_to_root_node(comment_form);
  initial_mark.appendChild(comment_form.get("root"));
}
