/*
@licstart The following is the entire license notice for the JavaScript code
in this page.

ChuspaLMS: Client hybridization used as substitute for the proprietary to
access learning management systems.
Copyright (C) 2020-2021, 2024  Adonay Felipe Nogueira <https://libreplanet.org/wiki/User:Adfeno> <adfeno.7046@gmail.com>

This file is part of ChuspaLMS.

ChuspaLMS is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

ChuspaLMS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with ChuspaLMS.  If not, see <https://www.gnu.org/licenses/>.

As additional permission under GNU GPL version 3 section 7, you may distribute non-source (e.g., minimized or compacted) forms of that code without the copy of the GNU GPL normally required by section 4, provided you include this license notice and a URL through which recipients can access the Corresponding Source.

@licend  The above is the entire license notice for the JavaScript code in this page.
*/

function get_origin_from_url_if_null(details) {
  for (let header of details.requestHeaders) {
    if (
        header.name.toLowerCase() == "origin" &&
        header.value == "null"
      ) {

      var url = new URL(details.url);
      header.value = url.origin +
        "/";

      break;
    }
  }

  return {requestHeaders: details.requestHeaders};
}

browser.webRequest.onBeforeSendHeaders.addListener(
  get_origin_from_url_if_null,
  {
    urls:
      [
        "*://lms.unimestre.com/*",
      ]
  },
  [
    "blocking",
    "requestHeaders",
  ]
);
