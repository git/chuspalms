/*
@licstart The following is the entire license notice for the JavaScript code
in this page.

ChuspaLMS: Client hybridization used as substitute for the proprietary to
access learning management systems.
Copyright (C) 2019-2021, 2024  Adonay Felipe Nogueira <https://libreplanet.org/wiki/User:Adfeno> <adfeno.7046@gmail.com>

This file is part of ChuspaLMS.

ChuspaLMS is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

ChuspaLMS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with ChuspaLMS.  If not, see <https://www.gnu.org/licenses/>.

As additional permission under GNU GPL version 3 section 7, you may distribute non-source (e.g., minimized or compacted) forms of that code without the copy of the GNU GPL normally required by section 4, provided you include this license notice and a URL through which recipients can access the Corresponding Source.

@licend  The above is the entire license notice for the JavaScript code in this page.
*/

window.onerror = alert;

var unstandardized_anchor_regexp_array = [
  /(?:^|\W+)(?:go[\W_]*url|popup[\W_]*doc|visualizar[\W_]*notas[\W_]*frequencias[\W_]*parciais)\(/i,
  /\s*["']([^"']+)["']\s*\)/i,
];

// @license magnet:?xt=urn:btih:c80d50af7d3db9be66a4d0a86db0286e4fd33292&dn=bsd-3-clause.txt BSD-3-Clause
/*
Copyright (c) 2005-2007 by Jared White & J. Max Wilson
Copyright (c) 2008-2010 by Joseph Woolley, Steffen Konerow, Jared White  & J. Max Wilson
Copyright (C) 2019, 2021  Adonay Felipe Nogueira
                                  <https://libreplanet.org/wiki/User:Adfeno>
                                                     <adfeno@hyperbola.info>

Based on https://github.com/Xajax/Xajax/blob/49ea682f4dffbda7a2984e7d07450207d233ad7c/xajax_js/xajax_core_uncompressed.js
*/

/*
Arguments:
  * xajax_base_request: non-array object with information about the base
    request. Changing any of the default values implies that you must redefine
    the defaults yourself as they will not be inherited.
    * location: string with URI.
    * properties: pure object holding the properties of the request.
      * method: string defining the method that will be used.
      * headers: non-array object with '"key": "value", …' pairs that you want
        to be be passed in the request.
      * body: string with body to be used for the request, usability depends on
        the method used.
  * request_commands: a pure object with 'xajax_command: "server_call", …'
    pairs, where the `xajax_command' comes from xajax itself and `server_call'
    depends on implementation.
  * request_arguments: 0-indexed array with arguments expected by
    request_commands.

Returned values:
  Success (and if request_commands is non-empty and supplied as non-array
  object with 'xajax_default_command: "server_call", …' pairs):
  * An enhanced request object.
*/
function prepare_request(
    xajax_base_request = {
      location: undefined,
      properties: {
        method: "GET",
        headers: {
          "If-Modified-Since": "Sat, 1 Jan 2000 00:00:00 GMT"
        },
        body: undefined,
      },
    },
    request_commands,
    request_arguments
  ) {

  if (! xajax_base_request ||
    Object.prototype.toString.call(xajax_base_request) == "[object Array]") {

    throw new TypeError(browser.i18n.getMessage(
      "typeerror_type_and_subtype_unexpected",
      [
        "xajax_base_request",
        Object.prototype.toString.call(xajax_base_request),
      ]
    ));
  } else if (! request_commands ||
    Object.prototype.toString.call(request_commands) != "[object Object]") {

    throw new TypeError(browser.i18n.getMessage(
      "typeerror_type_and_subtype_expected",
      [
        "request_commands",
        Object.prototype.toString.call(request_commands),
        "[object Object]",
      ]
    ));
  } else if (
      Object.prototype.toString.call(request_arguments)
        != "[object Undefined]" &&
      Object.prototype.toString.call(request_arguments)
        != "[object Array]" &&
      Object.prototype.toString.call(request_arguments)
        != "[object Object]"
    ) {

    throw new TypeError(browser.i18n.getMessage(
      "typeerror_type_and_subtype_expected",
      [
        "request_arguments",
        Object.prototype.toString.call(request_commands),
        "[object Undefined], [object Array], [object Object]",
      ]
    ));
  }

  xajax_base_request.properties.method =
    xajax_base_request.properties.method.toUpperCase() || "GET";

  var request_parameters = "";

  if (xajax_base_request.properties.method == "GET") {
    if (xajax_base_request.location.indexOf("?") == -1) {
      xajax_base_request.location += "?";
    } else {
      xajax_base_request.location += "&";
    }
  }

  for (let xajax_command in request_commands) {
    if (! request_commands.hasOwnProperty(xajax_command)) {
      continue;
    }

    if (xajax_command != "constructor") {
      request_parameters += xajax_command +
        "=" +
        encodeURIComponent(request_commands[xajax_command]) +
        "&";
    }
  }

  request_parameters += "xjxr=" + (new Date()).getTime();

  if (
      (
        Object.prototype.toString.call(request_arguments) == "[object Array]" &&
        request_arguments.length > 0
      ) ||
      (
        Object.prototype.toString.call(request_arguments)
          == "[object Object]" &&
        Object.getOwnPropertyNames(request_arguments).length > 0
      )
    ) {

    for (let argument_index in request_arguments) {
      request_parameters += "&xjxargs[]=" +
        encodeURIComponent(convert_to_XML(request_arguments[argument_index]));
    }
  }

  if (xajax_base_request.properties.method == "GET") {
    xajax_base_request.location += request_parameters;
  } else if (xajax_base_request.properties.method == "POST") {
    if (xajax_base_request.properties.body) {
      xajax_base_request.properties.body += "&" + request_parameters;
    } else {
      xajax_base_request.properties.body = "";
      xajax_base_request.properties.body += request_parameters;
    }
  }

  return xajax_base_request;
};
// @license-end

/*
Arguments:
  * raw_answers_form: form element filled with answers.
  * questions_properties: associative array holding the properties of each
    question.

Returned values:
  * Array with answer properties based on the form element types and content.
*/
function add_answers_to_questions_properties(raw_answers_form,
  questions_properties) {

  for (let step of
    raw_answers_form.querySelectorAll("fieldset[title^=\"Etapa\"]")) {

    for (let answer of step.querySelectorAll("[name^=\"resposta\\[\"]")) {
      if (
          answer.nodeName == "TEXTAREA" ||
          (
            answer.nodeName == "INPUT" &&
            /^(?:color|date|datetime-local|email|month|number|search|tel|text|time|url|week)$/i
              .test(answer.type)
          )
        ) {

        questions_properties[answer.name]["cd_alternativa"] = null;
        questions_properties[answer.name]["ds_resposta"] = answer.value;
      } else if (
          answer.nodeName == "SELECT" ||
          (
            answer.nodeName == "INPUT" &&
            /^(?:checkbox|radio|range)$/i.test(answer.type)
          )
        ) {

        questions_properties[answer.name]["cd_alternativa"] = answer.value;
        questions_properties[answer.name]["ds_resposta"] = null;
      }
    }
  }

  return questions_properties;
}

async function send_answers(
    questions_properties,
    event
  ) {

  event.preventDefault();
  var request;

  request = prepare_request(
    {
      location: document.location.href,
      properties: {
        method: "POST",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
          "If-Modified-Since": "Sat, 1 Jan 2000 00:00:00 GMT",
        }
      },
    },
    {
      xjxfun: "inserirRespostas",
    },
    add_answers_to_questions_properties(
      event.target,
      questions_properties
    )
  );

  var response = await fetch(
    request.location,
    request.properties
  );

  if (response.status == 200) {
    request = prepare_request(
      {
        location: document.location.href,
        properties: {
          method: "POST",
          headers: {
            "Content-Type": "application/x-www-form-urlencoded",
            "If-Modified-Since": "Sat, 1 Jan 2000 00:00:00 GMT",
          },
        },
      },
      {
        xjxfun: "finalizarAvaliacao",
      },
      [
        event.target.querySelector("#cd_pesquisado").value,
        2,
      ]
    );

    response = await fetch(
      request.location,
      request.properties
    );

    if (response.status == 200) {
      request = prepare_request(
        {
          location: document.location.href,
          properties: {
            method: "POST",
            headers: {
              "Content-Type": "application/x-www-form-urlencoded",
              "If-Modified-Since": "Sat, 1 Jan 2000 00:00:00 GMT",
            },
          },
        },
        {
          xjxfun: "redirecionarPesquisadoInicio",
        }
      );

      response = await fetch(
        request.location,
        request.properties
      );

      if (
          response.status == 200 &&
          /inicial\.php/i.test(await response.text())
        ) {

        alert(browser.i18n.getMessage("form_submission_succeeded"));
        document.location.href = "inicio.php" + document.location.search;
      } else {
        console.debug(
          request,
          response,
          await response.text()
        );
        throw new Error(browser.i18n.getMessage(
          "error_unhandled_response_status"
        ));
      }
    }
  } else {
    console.debug(
      request,
      response,
      await response.text()
    );
    throw new Error(browser.i18n.getMessage(
      "error_unhandled_response_status"
    ));
  }
}

async function get_available_year_semester_and_course(event) {
  if (event.target.form.elements["tipo_req"].value == 1) {
    var user_code;

    if (
        document.querySelector("#cd_responsabilizado") != null &&
        document.querySelector("#cd_responsabilizado").value.length > 0
      ) {

      user_code = document.querySelector("#cd_responsabilizado").value;
    } else if (
        document.querySelector("#cd_pessoa_outro") != null &&
        document.querySelector("#cd_pessoa_outro").value.length > 0
      ) {

      user_code = document.querySelector("#cd_pessoa_outro").value;
    } else {
      user_code = -1;
    }

    var request;

    request = prepare_request(
      {
        location: document.location.href,
        properties: {
          method: "POST",
          headers: {
            "Content-Type": "application/x-www-form-urlencoded",
            "If-Modified-Since": "Sat, 1 Jan 2000 00:00:00 GMT",
          },
        },
      },
      {
        xjxfun: "getAnoSemestreMatriculaCurso",
      },
      {
        cd_pessoa: user_code,
        cd_curso: event.target.form.elements["cd_filtro"].value,
      }
    );

    var response = await fetch(
      request.location,
      request.properties
    );

    if (response.status == 200) {
      response = await response.text();
      response = (new DOMParser()).parseFromString(response, "application/xml");
      event.target.form.elements["anosem"].value =
        response.querySelector("v").textContent.substring(1);
    }
  }
}

if (/^\/projetos\/portal_online\/index\.php/.test(document.location.pathname)) {
  document.querySelectorAll("a")
   .forEach(function modify_anchor(unstandardized_anchor) {
     var matching_attributes_values =
       get_matching_strings_from_selected_elements_attributes(
         unstandardized_anchor_regexp_array[0],
         unstandardized_anchor,
         [
           "href",
           [
             "data-librejs-blocked-event",
             undefined,
             "value",
           ],
         ]
       );

     if (
         Object.prototype.toString.call(matching_attributes_values)
           == "[object Map]" &&
         [
           ...matching_attributes_values.keys(),
         ].length
       ) {

       function fix_unstandardized_anchor(value) {
         if ((value instanceof Object)) {
           throw new TypeError(browser.i18n.getMessage(
             "typeerror_instanceof_unexpected",
             [
               value,
               "Object",
             ]
           ));
         }

         unstandardized_anchor.href = value.match(new RegExp(
           unstandardized_anchor_regexp_array.map(function get_regexp(part) {
             return part.source;
           }).join(""),
           "i"
         ))[1];
         unstandardized_anchor.target = "conteudo";
         unstandardized_anchor.rel = "noreferrer";
       }

       attribute_loop: for (let attribute of
         matching_attributes_values.entries()) {

         if (Object.prototype.toString.call(attribute[1]) == "[object Map]") {
           for (let subattribute of attribute[1].entries()) {
             fix_unstandardized_anchor(subattribute[1]);
             break attribute_loop;
           }
         } else {
           fix_unstandardized_anchor(attribute[1]);
           break attribute_loop;
         }
       }
     }
   });

  document.querySelectorAll(".menu-lateral")
    .forEach(function replace_menu(unstandardized_menu) {
      var simple_menu_toggle = new Map(
        [
          [
            "input",
            document.createElement("input"),
          ],
          [
            "label",
            document.createElement("label"),
          ],
        ]
      );

      simple_menu_toggle.get("input").id = "menu_toggle";
      simple_menu_toggle.get("input").type = "checkbox";
      simple_menu_toggle.get("input").className = manifest.name.toLowerCase();

      simple_menu_toggle.get("label").setAttribute(
        "for",
        simple_menu_toggle.get("input").id
      );
      simple_menu_toggle.get("label").innerHTML = "&#9776;";
    
      unstandardized_menu.parentNode.insertBefore(
        simple_menu_toggle.get("input"),
        unstandardized_menu
      );

      unstandardized_menu.querySelectorAll(".menu-lateral-topo")
        .forEach(function replace_toggle(unstandardized_toggle) {
          unstandardized_toggle.parentNode.replaceChild(
            simple_menu_toggle.get("label"),
            unstandardized_toggle
          );
        });
    });

} else if (
    /^\/projetos\/diario_classes\/notas_frequencias_listar\.php/
      .test(document.location.pathname)
  ) {

  document.querySelectorAll("a")
    .forEach(function replace_anchor(unstandardized_anchor) {
      var matching_attributes_values =
        get_matching_strings_from_selected_elements_attributes(
          unstandardized_anchor_regexp_array[0],
          unstandardized_anchor,
          [
            "onclick",
            [
              "data-librejs-blocked-event",
              undefined,
              "value",
            ],
          ]
        );

      if (
          Object.prototype.toString.call(matching_attributes_values)
            == "[object Map]" &&
          [
            ...matching_attributes_values.keys(),
          ].length
        ) {

        function replace_unstandardized_anchor(value) {
          if ((value instanceof Object)) {
            throw new TypeError(browser.i18n.getMessage(
              "typeerror_instanceof_unexpected",
              [
                value,
                "Object",
              ]
            ));
          }

          var anchor = new Map(
            [
              [
                "root",
                document.createDocumentFragment(),
              ],
              [
                "anchor",
                document.createElement("a"),
              ],
            ]
          );

          anchor.get("anchor").href = value.match(new RegExp(
            unstandardized_anchor_regexp_array.map(function get_regexp(part) {
              return part.source;
            }).join(""),
            "i"
          ))[1];
          anchor.get("anchor").innerText = unstandardized_anchor.innerText;

          append_map_entries_to_root_node(anchor);
          unstandardized_anchor.parentNode.replaceChild(
            anchor.get("root"),
            unstandardized_anchor
          );
        }

        attribute_loop: for (let attribute of
          matching_attributes_values.entries()) {

          if (
              Object.prototype.toString.call(attribute[1]) == "[object Map]"
            ) {

            for (let subattribute of attribute[1].entries()) {
              replace_unstandardized_anchor(subattribute[1]);
              break attribute_loop;
            }
          } else {
            replace_unstandardized_anchor(attribute[1]);
            break attribute_loop;
          }
        }
      }
    });
} else if (
    /^\/projetos\/matricula\/index\.php/.test(document.location.pathname)
  ) {

  document.querySelectorAll("[name=\"anosemestre\"]")
    .forEach(function replace_period_selector(period_selector) {
      var period_navigation = new Map(
        [
          [
            "root",
            document.createDocumentFragment(),
          ],
          [
            "navigation",
            new Map(
              [
                [
                  "root",
                  document.createElement("nav"),
                ],
                [
                  "list",
                  new Map(
                    [
                      [
                        "root",
                        document.createElement("ul"),
                      ],
                    ]
                  ),
                ],
              ]
            ),
          ],
        ]
      );

      period_selector.querySelectorAll("option")
        .forEach(function get_period(period) {
          period_navigation.get("navigation").get("list").set(
            period.innerText,
            new Map(
              [
                [
                  "root",
                  document.createElement("li"),
                ],
                [
                  "anchor",
                  document.createElement("a"),
                ],
              ]
            )
          );

          period_navigation.get("navigation").get("list")
            .get(period.innerText).get("anchor").href = period.value;
          period_navigation.get("navigation").get("list")
            .get(period.innerText).get("anchor").innerText = period.innerText;
        });

      append_map_entries_to_root_node(period_navigation);
      period_selector.parentNode.replaceChild(
        period_navigation.get("root"),
        period_selector
      );
    });

  document.querySelectorAll("div")
    .forEach(function replace_anchor(unstandardized_anchor) {
      var matching_attributes_values =
        get_matching_strings_from_selected_elements_attributes(
          unstandardized_anchor_regexp_array[0],
          unstandardized_anchor,
          [
            "onclick",
            [
              "data-librejs-blocked-event",
              undefined,
              "value",
            ],
          ]
        );

      if (
          Object.prototype.toString.call(matching_attributes_values) ==
            "[object Map]" &&
          [
            ...matching_attributes_values.keys(),
          ].length
        ) {

        function replace_unstandardized_anchor(value) {
          if ((value instanceof Object)) {
            throw new TypeError(browser.i18n.getMessage(
              "typeerror_instanceof_unexpected",
              [
                value,
                "Object",
              ]
            ));
          }

          var anchor = new Map(
            [
              [
                "root",
                document.createDocumentFragment(),
              ],
              [
                "anchor",
                document.createElement("a"),
              ],
            ]
          );

          anchor.get("anchor").innerText = browser.i18n.getMessage("view");
          anchor.get("anchor").href = value.match(new RegExp(
            unstandardized_anchor_regexp_array.map(function get_regexp(part) {
              return part.source;
            }).join(""),
            "i"
          ))[1];

          append_map_entries_to_root_node(anchor);
          unstandardized_anchor.parentNode.replaceChild(
            anchor.get("root"),
            unstandardized_anchor
          );
        }

        attribute_loop: for (let attribute of
          matching_attributes_values.entries()) {

          if (Object.prototype.toString.call(attribute[1]) == "[object Map]") {
            for (let subattribute of attribute[1].entries()) {
              replace_unstandardized_anchor(subattribute[1]);
              break attribute_loop;
            }
          } else {
            replace_unstandardized_anchor(attribute[1]);
            break attribute_loop;
          }
        }
      }
  });
} else if (
    /^\/projetos\/matricula\/proc\/index\.php/.test(document.location.pathname)
  ) {

  var unstandardized_submit_regexp = /(?:^|\W+)submeter\(/i;

  var old_request_form = document.forms["molForm"];
  var new_request_form = clone_element_remvoing_event_handlers_and_listeners(
    old_request_form,
    true
  );

  old_request_form.querySelectorAll("#av, #vl")
    .forEach(function replace_button(each_button) {
      if (each_button) {
        if (each_button instanceof HTMLAnchorElement) {
          var new_submit_button = document.createElement("button");
          new_submit_button.id = each_button.id;
          new_submit_button.innerText = each_button.innerText;
          new_request_form.querySelector("#" + each_button_id)
            .parentNode.replaceChild(
              new_submit_button,
              each_button
            );
        } else if (each_button instanceof HTMLInputElement) {
          new_request_form.querySelector("#" + each_button.id).type = "submit";
        }
      }

      function replace_unstandardized_request_action(
          each_button,
          value
        ) {

        if ((value instanceof Object)) {
          throw new TypeError(browser.i18n.getMessage(
            "typeerror_instanceof_unexpected",
            [
              value,
              "Object",
            ]
          ));
        }

        if (each_button.id == "av") {
          new_request_form.action = new_request_form.action +
            value.match(new RegExp(
              unstandardized_submit_regexp.source +
                /\s*["']([^"']*)["']\s*\)/.source,
              "i"
            ));
        } else if (each_button.id == "vl") {
          new_request_form.querySelector("#" + each_button.id).formaction =
            new_request_form.action.replace(/&avancar=true/i, "") +
            value.match(new RegExp(
              unstandardized_submit_regexp.source +
                /\s*["']([^"']*)["']\s*\)/.source,
              "i"
            ));
        }
      }

      var matching_attributes_values =
        get_matching_strings_from_selected_elements_attributes(
          unstandardized_submit_regexp,
          each_button,
          [
            "onclick",
            [
              "data-librejs-blocked-event",
              undefined,
              "value",
            ],
          ]
        );

      if (
          Object.prototype.toString.call(matching_attributes_values) ==
            "[object Map]" &&
          [
            ...matching_attributes_values.keys(),
          ].length
        ) {

        attribute_loop: for (let attribute of
          matching_attributes_values.entries()) {

          if (Object.prototype.toString.call(attribute[1]) == "[object Map]") {
            for (let subattribute of attribute[1].entries()) {
              replace_unstandardized_request_action(
                each_button,
                subattribute[1]
              );
              break attribute_loop;
            }
          } else {
            replace_unstandardized_request_action(each_button, attribute[1]);
            break attribute_loop;
          }
        }
      }
  });

  document.querySelectorAll("a")
    .forEach(function replace_anchor(unstandardized_anchor) {
      var matching_attributes_values =
        get_matching_strings_from_selected_elements_attributes(
          unstandardized_anchor_regexp_array[0],
          unstandardized_anchor,
          [
            "onclick",
            [
              "data-librejs-blocked-event",
              undefined,
              "value",
            ],
          ]
        );

      if (
          Object.prototype.toString.call(matching_attributes_values) ==
            "[object Map]" &&
          [
            ...matching_attributes_values.keys(),
          ].length
        ) {

        function replace_unstandardized_anchor(value) {
          if ((value instanceof Object)) {
            throw new TypeError(browser.i18n.getMessage(
              "typeerror_instanceof_unexpected",
              [
                value,
                "Object",
              ]
            ));
          }

          var anchor = new Map(
            [
              [
                "root",
                document.createDocumentFragment(),
              ],
              [
                "anchor",
                document.createElement("a"),
              ],
            ]
          );

          anchor.get("anchor").innerText = browser.i18n.getMessage("view");
          anchor.get("anchor").href = value.match(new RegExp(
            unstandardized_anchor_regexp_array.map(function get_regexp(part) {
              return part.source;
            }).join(""),
            "i"
          ))[1];

          append_map_entries_to_root_node(anchor);
          unstandardized_anchor.parentNode.replaceChild(
            anchor.get("root"),
            unstandardized_anchor
          );
        }

        attribute_loop: for (let attribute of
          matching_attributes_values.entries()) {

          if (Object.prototype.toString.call(attribute[1]) == "[object Map]") {
            for (let subattribute of attribute[1].entries()) {
              replace_unstandardized_anchor(subattribute[1]);
              break attribute_loop;
            }
          } else {
            replace_unstandardized_anchor(attribute[1]);
            break attribute_loop;
          }
        }
      }
  });

  old_request_form.parentNode.replaceChild(
    new_request_form,
    old_request_form
  );
} else if (
    /^\/projetos\/requerimentos\/index\.php/.test(document.location.pathname)
  ) {

  var unstandardized_redirect_regexp = /\W+redireciona[\W_]*solicitacao\(/i;

  document.querySelectorAll("a")
    .forEach(function replace_anchor(unstandardized_anchor) {
      var matching_attributes_values =
        get_matching_strings_from_selected_elements_attributes(
          unstandardized_redirect_regexp,
          unstandardized_anchor,
          [
            "onclick",
            [
              "data-librejs-blocked-event",
              undefined,
              "value",
            ],
          ]
        );

      if (
          Object.prototype.toString.call(matching_attributes_values) ==
            "[object Map]" &&
          [
            ...matching_attributes_values.keys(),
          ].length
        ) {

        function replace_unstandardized_anchor(value) {
          if ((value instanceof Object)) {
            throw new TypeError(browser.i18n.getMessage(
              "typeerror_instanceof_unexpected",
              [
                value,
                "Object",
              ]
            ));
          }

          var anchor_href_parts_array = [];

          for (let part of value.matchAll(/["']([^"']*)["']/g)) {
            anchor_href_parts_array.push(part[1]);
          }

          anchor_href_parts_array.splice(1, 0, "&menu=2&cd_req=");
          anchor_href_parts_array.splice(3, 0, "&cd_pessoa_selecionada=");
          anchor_href_parts_array.splice(5, 0, "&cd_grupo_selecionado=");

          var anchor = new Map(
            [
              [
                "root",
                document.createDocumentFragment(),
              ],
              [
                "anchor",
                document.createElement("a"),
              ],
            ]
          );

          anchor.get("anchor").innerText = unstandardized_anchor.innerText;
          anchor.get("anchor").href = anchor_href_parts_array.join("");

          append_map_entries_to_root_node(anchor);
          unstandardized_anchor.parentNode.parentNode.replaceChild(
            anchor.get("root"),
            unstandardized_anchor.parentNode
          );
        }

        attribute_loop: for (let attribute of
          matching_attributes_values.entries()) {

            if (
                Object.prototype.toString.call(attribute[1])
                  == "[object Map]"
              ) {

              for (let subattribute of attribute[1].entries()) {
                replace_unstandardized_anchor(subattribute[1]);
                break attribute_loop;
              }
            } else {
              replace_unstandardized_anchor(attribute[1]);
              break attribute_loop;
            }
        }
      }
  });

  if ((new URLSearchParams(document.location.search)).get("cd_req")) {
    var old_request_form = document.forms["frmRegistro"];
    var new_request_form = clone_element_remvoing_event_handlers_and_listeners(
      old_request_form,
      true
    );

    new_request_form.elements["cd_filtro"].addEventListener(
      "change",
      get_available_year_semester_and_course
    );

    for (let unstandardized_email_input of
      new_request_form.querySelectorAll(".mask-email")) {

      unstandardized_email_input.type = "email";
    }

    new_request_form.elements["ds_obs"].required = true;
    new_request_form.elements["anexo[]"].accept =
      new_request_form.elements["extensoesArquivo"].value.replace(
        /;([^\.])/g, ",.$1").replaceAll(/^([^\.])/g,
        ".$1"
      );

    new_request_form.elements["btnSend"].type = "submit";

    old_request_form.parentNode.replaceChild(
      new_request_form,
      old_request_form
    );
  }
} else if (
    /^\/projetos\/avaliacoes\/resolucao\.php/.test(document.location.pathname)
  ) {

  var questions_properties = [];
  var evaluation_form = document.querySelector("#formAvaliacao");

  evaluation_form.querySelector("#sn_final").value = 1;

  evaluation_form.querySelectorAll("input.finish[type=\"button\"]")
    .forEach(function replace_submit_button(unstandardized_submit_button) {
      var submit_button = new Map(
        [
          [
            "root",
            document.createDocumentFragment(),
          ],
          [
            "submit_button",
            document.createElement("button"),
          ],
        ]
      );

      submit_button.get("submit_button").type = "submit";
      submit_button.get("submit_button").innerText =
        browser.i18n.getMessage("submit");

      append_map_entries_to_root_node(submit_button);
      unstandardized_submit_button.parentNode.replaceChild(
        submit_button.get("root"),
        unstandardized_submit_button
      );
    });

  evaluation_form.querySelectorAll(
      "fieldset[title^=\"Etapa\"] [name^=\"resposta\\[\"]"
    ).forEach(function get_question_properties(question) {
      questions_properties[question.name] = [];
      var current_question = [];
      var question_name = question.name.replace(
        /^resposta\[\s*|\s*\]$/gi,
        ""
      ).split("_");
      var repetition_checker = question_name[1] || ".*";

      current_question["cd_pesquisado"] =
        evaluation_form.querySelector("#cd_pesquisado").value;
      current_question["cd_questao"] = question_name[0];
      current_question["cd_chave"] = question_name[1] || null;
      questions_properties[question.name] = current_question;
    });

  evaluation_form.addEventListener(
    "submit",
    send_answers.bind(
      null,
      questions_properties
    )
  );
} else if (
    /^\/projetos\/unimestre\/contato_email\/index\.php/
      .test(document.location.pathname) &&
    (new URLSearchParams(document.location.search)).getAll("acao")
      .some(function is_listing_contacts(action_value) {
        return action_value == "listar_contatos";
      })
  ) {

  /*
  [name="disciplinas[]"]
  name and value

  [name="disciplinas[]"] ~ a
  innerText

  transform to input
  */

  /*
  function toggle_checkbox_inputs(
      checkbox_input_selector_string,
      parent_selector_string,
      check_value = true,
      event
    ) {

    if (
        ! /\s+(?:Number|String)\]$/.test(
          Object.prototype.toString.call(checkbox_input_selector_string)
        ) ||
        (
          /\s+(?:Number|String)\]$/.test(
            Object.prototype.toString.call(checkbox_input_selector_string)
          ) &&
          ! checkbox_input_selector_string.length
        )
      ) {

      throw new TypeError(browser.i18n.getMessage(
        "typeerror_type_and_subtype_expected",
        [
          "checkbox_input_selector_string",
          "Number, String",
        ]
      ));
    }

    if (parent_selector_string) {
      var checkbox_input_elements = document.querySelectorAll(
        parent_selector_string +
        " > " +
        checkbox_input_selector_string
      );
    } else {
      var checkbox_input_elements = document.querySelectorAll(
        checkbox_input_selector_string
      );
    }

    for (let each_checkbox of checkbox_input_elements) {
      if (! (each_checkbox instanceof HTMLInputElement)) {
        throw new TypeError(browser.i18n.getMessage(
          "typeerror_instanceof_expected",
          [
            "each_checkbox",
            "HTMLInputElement",
          ]
        ));
      } else if (each_checkbox.type != "checkbox") {
        throw new TypeError(browser.i18n.getMessage(
          "typeerror_input_form_type_expected",
          [
            "each_checkbox",
            each_checkbox.type,
            "checkbox",
          ]
        ));
      }

      each_checkbox.checked = check_value || false;
    }
  }
  */

  document.querySelectorAll('[id^="turmas_"] input[type="checkbox"]')
    .forEach(function check_related_inputs(each_element) {
      each_element.checked = true;
    });
}
