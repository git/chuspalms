/*
@licstart The following is the entire license notice for the JavaScript code
in this page.

ChuspaLMS: Client hybridization used as substitute for the proprietary to
access learning management systems.
Copyright (C) 2019-2021, 2024  Adonay Felipe Nogueira <https://libreplanet.org/wiki/User:Adfeno> <adfeno.7046@gmail.com>

This file is part of ChuspaLMS.

ChuspaLMS is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

ChuspaLMS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with ChuspaLMS.  If not, see <https://www.gnu.org/licenses/>.

As additional permission under GNU GPL version 3 section 7, you may distribute non-source (e.g., minimized or compacted) forms of that code without the copy of the GNU GPL normally required by section 4, provided you include this license notice and a URL through which recipients can access the Corresponding Source.

@licend  The above is the entire license notice for the JavaScript code in this page.
*/

manifest = browser.runtime.getManifest();

common_event_handlers = [
  "onabort",
  "onanimationcancel",
  "onanimationend",
  "onanimationiteration",
  "onauxclick",
  "onblur",
  "oncancel",
  "oncanplay",
  "oncanplaythrough",
  "onchange",
  "onclick",
  "onclose",
  "oncontextmenu",
  "oncuechange",
  "ondblclick",
  "ondurationchange",
  "onended",
  "onerror",
  "onfocus",
  "onformdata",
  "ongotpointercapture",
  "oninput",
  "oninvalid",
  "onkeydown",
  "onkeypress",
  "onkeyup",
  "onload",
  "onloadeddata",
  "onloadedmetadata",
  "onloadend",
  "onloadstart",
  "onlostpointercapture",
  "onmousedown",
  "onmouseenter",
  "onmouseleave",
  "onmousemove",
  "onmouseout",
  "onmouseover",
  "onmouseup",
  "onpause",
  "onplay",
  "onplaying",
  "onpointercancel",
  "onpointerdown",
  "onpointerenter",
  "onpointerleave",
  "onpointermove",
  "onpointerout",
  "onpointerover",
  "onpointerup",
  "onreset",
  "onresize",
  "onscroll",
  "onselect",
  "onselectionchange",
  "onselectstart",
  "onsubmit",
  "ontouchcancel",
  "ontouchstart",
  "ontransitioncancel",
  "ontransitionend",
  "onwheel",
];

function get_cookie(key) {
  /*
  * Arguments:
    * key: non-empty string or number.

  * Result:
    * Success:
      * return: cookie's string value.
  */

  if (
      (
        Object.prototype.toString.call(key) != "[object String]" &&
        Object.prototype.toString.call(key) != "[object Number]"
      ) ||
      (
        (
          Object.prototype.toString.call(key) == "[object String]" ||
          Object.prototype.toString.call(key) == "[object Number]"
        ) &&
        ! key.length
      )
    ) {

    throw new TypeError(browser.i18n.getMessage(
      "typeerror_type_and_subtype_expected",
      [
        "key",
        Object.prototype.toString.call(key),
        "[object String], [object Number]",
      ]
    ));
  }

  return document.cookie.match(new RegExp(
    "(?:(?:^|.*;\\\s*)" +
    key +
    "\\\s*=\\\s*([^;]*).*$)|^.*$"
  ))[1];
}

function remove_element(element) {
  /*
  Arguments:
    * element: any instance of Element.
  */

  if (! (element instanceof Element)) {
    throw new TypeError(browser.i18n.getMessage(
      "typeerror_instanceof_expected",
      [
        "element",
        "Element",
      ]
    ));
  }

  element.remove();
}

function clone_element_remvoing_event_handlers_and_listeners(
    input_element,
    operate_on_children_boolean = true
  ) {
  /*
  Arguments:
    * input_element: any instance of HTMLElement.
    * operate_on_children_boolean: Boolean indicating if children should also
      be cloned and operated on independently.
  */

  if (! (input_element instanceof HTMLElement)) {
    throw new TypeError(browser.i18n.getMessage(
      "typeerror_instanceof_expected",
      [
        "input_element",
        "HTMLElement",
      ]
    ));
  } else if (typeof(operate_on_children_boolean) != "boolean") {

    throw new TypeError(browser.i18n.getMessage(
      "typeerror_instanceof_expected",
      [
        "operate_on_children_boolean",
        "Boolean",
      ]
    ));
  }

  var output_element;
  if (operate_on_children_boolean == false) {
    output_element = input_element;
  } else {
    output_element = input_element.cloneNode(true);
  }

  common_event_handlers.forEach(function remove_handler(each_handler) {
    output_element.removeAttribute(each_handler);
  });

  for (let inner_element of output_element.children) {
    clone_element_remvoing_event_handlers_and_listeners(
      inner_element,
      false
    );
  }

  return output_element;
}

function convert_object_to_string(object) {
  var string_content = {};
  for (let key_name in object)
  if (! (object[key_name] instanceof Object)) {
    string_content[key_name] = object[key_name];
  }

  return string_content;
}

function convert_request_or_response_to_object(request_or_response) {
  if (
      ! request_or_response instanceof Request &&
      ! request_or_response instanceof Response
    ) {

    throw new TypeError(browser.i18n.getMessage(
      "typeerror_instanceof_expected",
      [
        "request_or_response",
        "Request, Response",
      ]
    ));
  }

  return {
    ...convert_object_to_string(request_or_response),
    headers: Object.fromEntries(request_or_response.headers),
    signal: convert_object_to_string(request_or_response.signal),
  };
}

function get_object_properties_by_name_regexp(
    target_object,
    name_regexp
  ) {
  /*
  * Arguments:
    * target_object: any pure Object.
    * name_regexp: RegExp to look up.

  * Result:
    * return: first property whose name matches name_regexp.
    * console debug: name_regexp, all matches and the returned one.
  */

  if (Object.prototype.toString.call(target_object) != "[object Object]") {
    throw new TypeError(browser.i18n.getMessage(
      "typeerror_type_and_subtype_expected",
      [
        "target_object",
        Object.prototype.toString.call(target_object),
        "[object Object]",
      ]
    ));
  } else if (
      Object.prototype.toString.call(name_regexp) != "[object RegExp]"
    ) {

    throw new TypeError(browser.i18n.getMessage(
      "typeerror_type_and_subtype_expected",
      [
        "name_regexp",
        Object.prototype.toString.call(name_regexp),
        "[object RegExp]",
      ]
    ));
  }

  var array_with_matches = Object.keys(target_object)
    .filter(function get_matches(property_name) {

    return name_regexp.test(property_name);
  });

  console.debug(
    "name_regexp: " +
      name_regexp,
    "array_with_matches: " +
      array_with_matches,
    "return: " +
      array_with_matches[0]
  );
  return array_with_matches[0];
}

function get_common_property_names_from_member_object(
    common_property_names,
    member_object
  ) {
  /*
  * Arguments:
    * common_property_names: pure Object with each key set to an Array for
      which:
      * if length of 1 or 2: [0] must be a RegExp to look up against each
        object properties' name; and [1], either undefined or a string
        matching the name found.
      * if length of 3: same as before, but [2] must be another iteration of
        common_property_names.
    * member_object: any pure Object.

  * Result:
    * If a property name in member_object matches any iteration of
      common_property_names[0]: sets [1] of the corresponding iteration to the
      match.
  */

  for (let each_property_name in common_property_names) {
    if (
        Object.prototype.toString.call(
          common_property_names[each_property_name]
        ) != "[object Array]" ||
        (
          Object.prototype.toString.call(
            common_property_names[each_property_name]
          ) == "[object Array]" &&
          ! common_property_names[each_property_name].length
        )
      ) {

      throw new TypeError(browser.i18n.getMessage(
        "typeerror_type_and_subtype_expected",
        [
          "common_property_names[each_property_name]",
          Object.prototype.toString.call(
            common_property_names[each_property_name]
          ),
          "[object Array]",
        ]
      ));
    }

    if (common_property_names[each_property_name].length > 3) {
      console.warn(browser.i18n.getMessage(
        "warn_ignored_arguments",
        [
          "common_property_names[each_property_name]",
          common_property_names[each_property_name].length,
          ">= 1 && <= 3",
        ]
      ));
    }

    if (typeof(common_property_names[each_property_name][1]) == "undefined") {
      common_property_names[each_property_name][1] =
        get_object_properties_by_name_regexp(
          member_object,
          common_property_names[each_property_name][0]
      );
    }

    if (
        typeof(common_property_names[each_property_name][1]) != "undefined" &&
        Object.prototype.toString.call(
          common_property_names[each_property_name][2]
        ) == "[object Object]" &&
        Object.prototype.toString.call(
          member_object[common_property_names[each_property_name][1]]
        ) == "[object Object]"
      ) {

      get_common_property_names_from_member_object(
        common_property_names[each_property_name][2],
        member_object[common_property_names[each_property_name][1]]
      );
    }
  }
}

function append_map_entries_to_root_node(map) {
  /*
  * Arguments:
    * map: a Map of the following structure:
      * key 0: must be named as "root" and must be a Node.
      * other keys: either a Node or a Map with the same structure of map.

  * Result:
    * Modifies map to append each node to map.get("root").
    * return: true.
  */

  if (Object.prototype.toString.call(map) != "[object Map]") {
    throw new TypeError(browser.i18n.getMessage(
      "typeerror_type_and_subtype_expected",
      [
        "map",
        Object.prototype.toString.call(map),
        "[object Map]",
      ]
    ));
  } else if (! (map.get("root") instanceof Node)) {

    throw new TypeError(browser.i18n.getMessage(
      "typeerror_instanceof_expected",
      [
        "map.get(\"root\")",
        "Node",
      ]
    ));
  } else if (map.get("root").parentNode) {
    throw new TypeError(browser.i18n.getMessage(
      "typeerror_node_has_parent",
      [
        "map.get(\"root\")",
      ]
    ));
  }

  for (let element of map.entries()) {
    if (
        element[0] != "root" &&
        ! (element[1] instanceof Node) &&
        ! (element[1] instanceof Map)
      ) {

      throw new TypeError(browser.i18n.getMessage(
        "typeerror_instanceof_expected",
        [
          "element[1]",
          "Node, Map",
        ]
      ));
    } else if (
        element[0] != "root" &&
        element[1].parentNode
      ) {

      throw new TypeError(browser.i18n.getMessage(
        "typeerror_node_has_parent",
        [
          "element[1]",
        ]
      ));
    }

    if (element[0] != "root") {
      if (element[1] instanceof Node) {
        map.get("root").appendChild(element[1]);
      } else {
        append_map_entries_to_root_node(element[1]);
        map.get("root").appendChild(element[1].get("root"));
      }
    }
  }

  return true;
}

function set_object_or_array_property_by_string(
    target_object_or_array,
    string_with_selectors,
    value
  ) {
  /*
  * Arguments:
    * target_object_or_array: pure Object or pure Array.
    * string_with_selectors: non-empty string in the form of
      "level_1[level_2][level_3]".
    * value: any desired value.

  * Result:
    * Success:
      * Modifies target_object_or_array to set last member of
        string_with_selectors to value.
      * return: value.
  */

  if (
      Object.prototype.toString.call(target_object_or_array)
        != "[object Array]" &&
      Object.prototype.toString.call(target_object_or_array)
        != "[object Object]"
    ) {

    throw new TypeError(browser.i18n.getMessage(
      "typeerror_type_and_subtype_expected",
      [
        "target_object_or_array",
        "[object Array], [object Object]",
      ]
    ));
  } else if (
      Object.prototype.toString.call(string_with_selectors)
        != "[object String]" ||
      (
        Object.prototype.toString.call(string_with_selectors)
          == "[object String]" &&
        ! string_with_selectors.length
      )
    ) {

    throw new TypeError(browser.i18n.getMessage(
      "typeerror_type_and_subtype_expected",
      [
        "string_with_selectors",
        "[object String]",
      ]
    ));
  }

  var selectors = string_with_selectors.match(/(^[^\[\]]+)(\[.*\]$)*/);
  if (! selectors) {
    throw new SyntaxError(browser.i18n.getMessage(
      "syntaxerror_invalid_selectors_string",
      [
        "string_with_selectors",
        string_with_selectors,
      ]
    ));
  }

  selectors.splice(0, 1);

  if (selectors[1]) {
    if (/([\[\]])[^\[\]]*\1|\][^\[\]]+\[|\[\]/.test(selectors[1])) {
      throw new SyntaxError(browser.i18n.getMessage(
        "syntaxerror_invalid_selectors_string",
        [
          "string_with_selectors",
          string_with_selectors,
        ]
      ));
    }

    selectors = [
      selectors[0],
      ...Array.from(selectors[1].matchAll(/\[([^\[\]]+)\]/g),
        function get_sublevels_selectors(match) {
          return match[1];
      }),
    ];
  }

  for (let selector in selectors) {
    if (! selectors.hasOwnProperty(selector)) {
      continue;
    }

    if (
        selector == "groups" ||
        selector == "index" ||
        selector == "input"
      ) {

      delete selectors[selector];
    } else if (typeof(selectors[selector]) == "undefined") {

      selectors.splice(selector, 1);
    }
  }

  return selectors.reduce(function set_value(
        each_object,
        key,
        nesting_level
      ) {

      if (
          nesting_level + 1 != selectors.length &&
          typeof(each_object[key]) == "undefined"
        ) {

        each_object[key] = {};
      } else if (nesting_level + 1 == selectors.length) {
        each_object[key] = value;
      }

      return each_object[key];
    },
    target_object_or_array
  );
}

function get_object_or_array_property_by_string(
    target_object_or_array,
    string_or_array_with_selectors
  ) {
  /*
  * Arguments:
    * target_object_or_array: pure Object or pure Array.
    * string_or_array_with_selectors: either one of the following:
      * non-empty Array with each member being either a non-empty string,
        number or undefined. Occurrences of undefined must be followed by a
        defined (string or number) member.
      * non-empty string in the form of "level_1[level_2][level_3]". This
        forbids specifying a member as undefined.

  * Result:
    * Success:
      * return:
        * If any member of string_or_array_with_selectors is undefined: a Map
          with a list of names that match the next defined member of
          string_or_array_with_selectors, where the key in the resulting Map
          is set to that next defined member of string_or_array_with_selectors.
        * If no member of string_or_array_with_selectors is undefined: the
          matching part of target_object_or_array.
  */

  if (
      Object.prototype.toString.call(target_object_or_array)
        != "[object Array]" &&
      Object.prototype.toString.call(target_object_or_array)
        != "[object Object]"
    ) {

    throw new TypeError(browser.i18n.getMessage(
      "typeerror_type_and_subtype_expected",
      [
        "target_object_or_array",
        "[object Array], [object Object]",
      ]
    ));
  } else if (
      (
        Object.prototype.toString.call(string_or_array_with_selectors)
          != "[object String]" &&
        Object.prototype.toString.call(string_or_array_with_selectors)
          != "[object Array]"
      ) ||
      (
        (
          Object.prototype.toString.call(string_or_array_with_selectors)
            == "[object String]" ||
          Object.prototype.toString.call(string_or_array_with_selectors)
            == "[object Array]"
        ) &&
        ! string_or_array_with_selectors.length
      )
    ) {

    throw new TypeError(browser.i18n.getMessage(
      "typeerror_type_and_subtype_expected",
      [
        "string_or_array_with_selectors",
        "[object String], [object Array]",
      ]
    ));
  }

  if (
      Object.prototype.toString.call(string_or_array_with_selectors)
        == "[object String]"
    ) {

    var selectors = string_or_array_with_selectors.match(
      /(^[^\[\]]+)(\[.*\]$)*/
    );

    if (! selectors) {
      throw new SyntaxError(browser.i18n.getMessage(
        "syntaxerror_invalid_selectors_string",
        [
          "string_or_array_with_selectors",
          string_or_array_with_selectors,
        ]
      ));
    }

    selectors.splice(0, 1);

    if (selectors[1]) {
      if (/([\[\]])[^\[\]]*\1|\][^\[\]]+\[|\[\]/.test(selectors[1])) {
        throw new SyntaxError(browser.i18n.getMessage(
          "syntaxerror_invalid_selectors_string",
          [
            "string_or_array_with_selectors",
            string_or_array_with_selectors,
          ]
        ));
      }

      selectors = [
        selectors[0],
        ...Array.from(selectors[1].matchAll(/\[([^\[\]]+)\]/g),
          function get_sublevels_selectors(match) {
            return match[1];
        }),
      ];
    }

    for (let selector in selectors) {
      if (! selectors.hasOwnProperty(selector)) {
        continue;
      }

      if (selector == "index" ||
        selector == "input") {

        delete selectors[selector];
      } else if (! selectors[selector]) {

        selectors.splice(selector, 1);
      }
    }
  } else if (
      Object.prototype.toString.call(
        string_or_array_with_selectors
      ) == "[object Array]"
    ) {

    var problematic_member_type_and_subtype;
    if (string_or_array_with_selectors.some(function(some_member) {
      if (
          (
            Object.prototype.toString.call(some_member) != "[object String]" &&
            Object.prototype.toString.call(some_member) != "[object Number]" &&
            Object.prototype.toString.call(
              some_member
            ) != "[object Undefined]"
          ) ||
          (
            (
              Object.prototype.toString.call(some_member)
                == "[object String]" ||
              Object.prototype.toString.call(some_member)
                == "[object Number]"
            ) &&
            ! some_member.length
          )
        ) {

        problematic_member_type_and_subtype =
          Object.prototype.toString.call(some_member);

        return true;
      }
    })) {

      throw new TypeError(browser.i18n.getMessage(
        "typeerror_type_and_subtype_expected",
        [
          "string_or_array_with_selectors[some_member]",
          problematic_member_type_and_subtype,
          "[object String], [object Number], [object Undefined]",
        ]
      ));
    }

    selectors = string_or_array_with_selectors.slice(0);
  }

  return selectors.reduce(function get_value(
        each_object,
        key,
        nesting_level
      ) {

      if (typeof(key) == "undefined" &&
          (
            (
              Object.prototype.toString.call(each_object) != "[object Map]" &&
              (
                each_object.length ||
                Object.getOwnPropertyNames(each_object).length > 0
              )
            ) ||
            (
              Object.prototype.toString.call(each_object) == "[object Map]" &&
              [
                ...each_object.keys(),
              ].length
            )
          ) &&
          (
            nesting_level + 1 != selectors.length &&
            typeof(selectors[nesting_level + 1]) != "undefined"
          )
        ) {

        var matches_found = new Map();

        function set_found_matches(
            key,
            value
          ) {

          if (typeof(value) != "undefined") {
            if (Object.prototype.toString.call(each_object)
              != "[object Map]") {

              matches_found.set(key,
                value);
            } else {
              matches_found[key] = value;
            }
          }
        }

        if (Object.prototype.toString.call(each_object) != "[object Map]") {
          for (let each_key in each_object) {
            if (! each_object.hasOwnProperty(each_key)) {
              continue;
            }

            set_found_matches(
              each_key,
              each_object[each_key][selectors[nesting_level + 1]]
            );
          }
        } else {
          for (let each_key of each_object) {
            set_found_matches(
              each_key[0],
              each_key[1][selectors[nesting_level + 1]]
            );
          }
        }

        selectors.splice(nesting_level + 1, 1);

        if (
            [
              ...matches_found.keys(),
            ].length
          ) {

          return matches_found;
        }
      }

      if (Object.prototype.toString.call(
          each_object
        ) != "[object Map]") {

        return each_object.get(key);
      } else {
        return each_object[key];
      }        
    },
    target_object_or_array);
}

function get_matching_strings_from_selected_elements_attributes(
    input_regexp,
    target_element,
    target_attributes
  ) {
  /*
  * Arguments:
    * input_regexp: RegExp to compare against the values of target_attributes.
    * target_element: an Element object whose attributes will be searched for.
    * target_attributes: an Array whose values must be either a String or an
      Array of Strings.

  * Result:
    * If any match is found:
      * return: a Map whose entry names correspond to each String member of
        target_attributes (or first String member of the Array in each
        target_attributes) for each matching element value. The Map entries'
        values are the entire value of each match found.
  */

  if (! (target_element instanceof Element)) {
    throw new TypeError(browser.i18n.getMessage(
      "typeerror_instanceof_expected",
      [
        "target_element",
         "Element",
       ]
     ));
  }

  var tested_attributes_values = new Map();

  for (let each_attribute of target_attributes) {
    if (
        (
          Object.prototype.toString.call(
            each_attribute
          ) != "[object String]" &&
          Object.prototype.toString.call(
            each_attribute
          ) != "[object Array]"
        ) ||
        (
          (
            Object.prototype.toString.call(
              each_attribute
            ) == "[object String]" ||
            Object.prototype.toString.call(
              each_attribute
            ) == "[object Array]"
          ) &&
          ! each_attribute.length
        )
      ) {

      throw new TypeError(browser.i18n.getMessage(
        "typeerror_type_and_subtype_expected",
        [
          "each_attribute",
          Object.prototype.toString.call(each_attribute),
          "[object String], [object Array]",
        ]
      ));
    }

    if (
        Object.prototype.toString.call(each_attribute) == "[object String]" &&
        target_element.hasAttribute(each_attribute)
      ) {
      
      if (input_regexp.test(target_element.getAttribute(each_attribute))) {
        tested_attributes_values.set(
          each_attribute,
          target_element.getAttribute(each_attribute)
        );
      }
    } else if (
        Object.prototype.toString.call(each_attribute)
          == "[object Array]" &&
        target_element.hasAttribute(each_attribute[0])
      ) {

      tested_attributes_values.set(
        each_attribute[0],
        get_object_or_array_property_by_string(
          JSON.parse(target_element.getAttribute(each_attribute[0])),
          each_attribute.slice(1)
        )
      );

      if (
          Object.prototype.toString.call(tested_attributes_values
            .get(each_attribute[0])
          ) == "[object Map]" &&
          [
            ...tested_attributes_values.get(each_attribute[0]).keys(),
          ].length
        ) {

        tested_attributes_values.set(
          each_attribute[0],
          new Map(
            [
              ...tested_attributes_values.get(each_attribute[0]),
            ].filter(function test_subvalue(each_member) {
              return (! (each_member[1] instanceof Object) &&
                input_regexp.test(each_member[1]));
            })
          )
        );
        if (
            ! [
              ...tested_attributes_values.get(each_attribute[0]).keys(),
            ].length
          ) {

          tested_attributes_values.delete(each_attribute[0]);
        }
      }
    }  
  }

  if (
      [
        ...tested_attributes_values.keys()
      ].length
    ) {

    return tested_attributes_values;
  }
}

function add_object_to_formdata(
    input_object,
    target_formdata = new FormData(),
    entry_key_prefix
  ) {
  /*
  * Arguments:
    * input_object: any object (except FormData) to append to target_formdata.
    * target_formdata (optional): a FormData object.
    * entry_key_prefix (optional): to set as start of each key name of
      input_object.

  * Result:
    * return: modified target_formdata with input_object inside.
  */

  if (
      Object.prototype.toString.call(input_object) == "[object FormData]"
    ) {

    throw new TypeError(browser.i18n.getMessage(
      "typeerror_type_and_subtype_unexpected",
      [
        "input_object",
        Object.prototype.toString.call(input_object),
      ]
    ));
  } else if (
      Object.prototype.toString.call(target_formdata) != "[object FormData]"
    ) {

    throw new TypeError(browser.i18n.getMessage(
      "typeerror_type_and_subtype_expected",
      [
        "target_formdata",
        Object.prototype.toString.call(target_formdata),
        "[object FormData]",
      ]
    ));
  }

  for (let property_name in input_object) {
    if (! input_object.hasOwnProperty(property_name)) {
      continue;
    }

    var entry_key = (entry_key_prefix ?
        entry_key_prefix +
        "[" +
        property_name +
        "]":
      property_name
    );

    if (
        input_object[property_name] === null ||
        input_object[property_name] instanceof Date ||
        input_object[property_name] instanceof File ||
        typeof(input_object[property_name]) != "object"
      ) {

      target_formdata.append(
        entry_key,
        (input_object[property_name] !== null ?
          input_object[property_name] :
          "")
      );
    } else if (input_object[property_name] instanceof Array) {
      for (let member_index in input_object[property_name]) {
        if (! input_object[property_name].hasOwnProperty(member_index)) {
          continue;
        }

        target_formdata.append(
          entry_key,
          input_object[property_name][member_index]
        );
      }
    } else {
      add_object_to_formdata(
        input_object[property_name],
        target_formdata,
        entry_key
      );
    }
  }

  return target_formdata;
}

function add_formdata_to_object(
    input_formdata,
    target_object = {},
    entry_key_prefix
  ) {
  /*
  * Arguments:
    * input_formdata: a FormData object to append to target_object.
    * target_object (optional): any object (except FormData).
    * entry_key_prefix (optional): to set as start of each key name of
      input_formdata.

  * Result:
    * return: modified target_object with input_formdata inside.
  */

  if (Object.prototype.toString.call(input_formdata) != "[object FormData]") {
    throw new TypeError(browser.i18n.getMessage(
      "typeerror_type_and_subtype_expected",
      [
        "input_formdata",
        Object.prototype.toString.call(input_formdata),
        "[object FormData]",
      ]
    ));
  } else if (
      Object.prototype.toString.call(target_object) == "[object FormData]"
    ) {

    throw new TypeError(browser.i18n.getMessage(
      "typeerror_type_and_subtype_unexpected",
      [
        "target_object",
        Object.prototype.toString.call(target_object),
      ]
    ));
  }

  for (let entry_key of (new Set([...input_formdata]
      .map(function get_entry_key(entry) {
        return entry[0];
      })
    ))) {

    var search_entry_key = (entry_key_prefix ?
        entry_key_prefix +
        "[" +
        entry_key +
        "]" :
      entry_key);

    set_object_or_array_property_by_string(
      target_object,
      search_entry_key,
      input_formdata.getAll(entry_key)
    );
  }

  return target_object;
}

function convert_date_and_time_string_to_date_object(
    input_date_string,
    input_time_string
  ) {
  /*
  * Arguments:
    * input_date_string: a string in the form of "DD/MM/YYYY".
    * input_time_string (optional): a string in the form of either "HH",
      "HH:MM", "HH:MM:SS", "HH:MM:SS.SSS" or "HH:MM:SS,SSS".

  * Result:
    * return: a Date object.
  */

  if (input_date_string) {
    raw_date = input_date_string.split("/").reverse();

    if (raw_date.length == 2 ||
      raw_date.length == 3) {

      --raw_date[1];
      if (raw_date.length == 2) {
        raw_date.push(1);
      }
    } else {
      throw new SyntaxError(browser.i18n.getMessage(
        "syntaxerror_invalid_date_or_time_string",
        [
          "input_date_string",
          input_date_string,
        ]
      ));
    }
  }

  if (input_time_string) {
    raw_time = input_time_string.split(":");
    if (raw_time.length == 3) {
      var split_second = raw_time[2].split(/[\.,]/);
      if (split_second.length == 2) {
        raw_time.splice(2, 2, ...split_second);
      } else if (split_second.length > 2) {
        throw new SyntaxError(browser.i18n.getMessage(
          "syntaxerror_invalid_date_or_time_string",
          [
            "input_time_string",
            input_time_string,
          ]
        ));
      }
    } else if (raw_time.length > 3) {
      throw new SyntaxError(browser.i18n.getMessage(
        "syntaxerror_invalid_date_or_time_string",
        [
          "input_time_string",
          input_time_string,
        ]
      ));
    }

    return new Date(
      ...raw_date,
      ...raw_time
    );
  } else {
    return new Date(...raw_date);
  }
}

function preview_textarea_in_article(
    textarea,
    article,
    event
  ) {
  /*
  * Arguments:
    * textarea: HTMLTextAreaElement with plain text to set as content of
      article.
    * article: HTMLElement (preferably one of nodeName == "ARTICLE") which
      will be used to preview any HTML output of textarea. 
    * event (optional, unused): an Event fired.

  * Result:
    * A modified article with HTML output of textarea as its value.
  */

  if (! (textarea instanceof HTMLTextAreaElement)) {
    throw new TypeError(browser.i18n.getMessage(
      "typeerror_instanceof_expected",
      [
        "textarea",
        "HTMLTextAreaElement",
      ]
    ));
  } else if (! (article instanceof HTMLElement)) {
    throw new TypeError(browser.i18n.getMessage(
      "typeerror_instanceof_expected",
      [
        "article",
        "HTMLElement",
      ]
    ));
  }

  article.innerHTML = textarea.value;
}

// @license magnet:?xt=urn:btih:c80d50af7d3db9be66a4d0a86db0286e4fd33292&dn=bsd-3-clause.txt BSD-3-Clause
/*
Copyright (c) 2005-2007 by Jared White & J. Max Wilson
Copyright (c) 2008-2010 by Joseph Woolley, Steffen Konerow, Jared White & J. Max Wilson
Copyright (C) 2019, 2021  Adonay Felipe Nogueira
                                   <https://libreplanet.org/wiki/User:Adfeno>
                                                      <adfeno@hyperbola.info>

Based on https://github.com/Xajax/Xajax/blob/49ea682f4dffbda7a2984e7d07450207d233ad7c/xajax_js/xajax_core_uncompressed.js
*/

function convert_to_XML(
    subject,
    current_object_depth = 0,
    maximum_object_depth = 20,
    current_object_size = 0,
    maximum_object_size = 2000
  ) {
  /*
  * Arguments:
    * subject: anything you want to convert to XML.
    * current_object_depth: if this is a nested call, this is the current
      iteration inside the subject (as integer).
    * maximum_object_depth: maximum depth (as integer) that this function
      should use when converting the subject.
    * current_object_size: number of the current property in the subject (as
      integer).
    * maximum_object_size: limits the total number of properties the subject
      will hold when converted (as integer).

  * Result:
    * Success:
      * return:
        * If subject is not a string which has special characters: string with
          a structure which is a XML document used to make the xajax requests.
        * If subject is a string which has special characters: the subject as
          character data ("CDATA") representation.
    * Failure:
      * return: the subject itself.
  */
  
  if (subject !== null &&
    typeof("subject") == "object") {

    var XML_document = [];
    XML_document.push("<xjxobj>");

    for (let property_name in subject) {
      if (! subject.hasOwnProperty(property_name)) {
        continue;
      }

      ++current_object_size;

      if (current_object_size > maximum_object_size) {
        return XML_document.join("");
      }

      if (typeof(subject[property_name]) != "undefined") {
        if (
            property_name == "constructor" ||
            typeof(subject[property_name]) == "function"
          ) {

          return;
        }

        XML_document.push("<e><k>");
        XML_document.push(convert_to_XML(property_name));
        XML_document.push("</k><v>");

        if (
            subject[property_name] !== null &&
            typeof(subject[property_name]) == "object"
          ) {

          ++current_object_depth;

          if (current_object_depth <= maximum_object_depth) {
            XML_document.push(convert_to_XML(
              subject[property_name],
              current_object_depth,
              maximum_object_depth,
              current_object_size,
              maximum_object_size
            ));
          }

          --current_object_depth;
        } else {
          var non_object_value = convert_to_XML(subject[property_name]);

          if (
              typeof(non_object_value) == "undefined" ||
              non_object_value === null
            ) {

            XML_document.push("*");
          } else {
            switch (typeof(non_object_value)) {
              case "string":
                XML_document.push("S");
                break;
              case "boolean":
                XML_document.push("B");
                break;
              case "number":
                XML_document.push("N");
                break;
            }

            XML_document.push(non_object_value);
          }
        }

        XML_document.push("</v></e>");
      }
    }

    XML_document.push("</xjxobj>");
    return XML_document.join("");
  } else if (
      typeof(subject) == "string" &&
      encodeURIComponent(subject) != subject
    ) {

    var character_data = [];
    var character_data_initiators = subject.split("<![CDATA[");

    for (let initiator_index in character_data_initiators) {
      if (! character_data_initiators.hasOwnProperty(initiator_index)) {
        continue;
      }

      var character_data_contents =
        character_data_initiators[initiator_index].split(']]>');
      var partial_character_data = "";

      for (let content_index in character_data_contents) {
        if (
            ! character_data_contents.hasOwnProperty(content_index)
          ) {

          continue;
        }

        if (content_index != 0) {
          partial_character_data += "]]]]><![CDATA[>";
        }

        partial_character_data += character_data_contents[content_index];
      }

      if (initiator_index != 0) {
        character_data.push("<![]]><![CDATA[CDATA[");
      }

      character_data.push(partial_character_data);
    }

    return "<![CDATA[" + character_data.join("") + "]]>";
  } else {
    return subject;
  }
}
// @license-end
