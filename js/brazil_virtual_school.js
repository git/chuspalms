/*
@licstart The following is the entire license notice for the JavaScript code
in this page.

ChuspaLMS: Client hybridization used as substitute for the proprietary to
access learning management systems.
Copyright (C) 2019-2021, 2024  Adonay Felipe Nogueira <https://libreplanet.org/wiki/User:Adfeno> <adfeno.7046@gmail.com>

This file is part of ChuspaLMS.

ChuspaLMS is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

ChuspaLMS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with ChuspaLMS.  If not, see <https://www.gnu.org/licenses/>.

As additional permission under GNU GPL version 3 section 7, you may distribute non-source (e.g., minimized or compacted) forms of that code without the copy of the GNU GPL normally required by section 4, provided you include this license notice and a URL through which recipients can access the Corresponding Source.

@licend  The above is the entire license notice for the JavaScript code in this page.
*/

window.onerror = alert;

var unstandardized_anchor_regexp_array = [
  /(?:^|\W+)(?:encerrar[\W_]*inscricao)\(/i,
  /\s*["']?([^"']+)["']?\s*\)/i,
];

var common_request_properties = {
  method: "POST",
}

async function update_municipalities_from_federative_unit_select_element(
    municipality_select_element_string,
    select_element_in_same_form_as_event = true,
    event
  ) {

  /*
  * Arguments:
    * municipality_select_element_string: a String or Number that must suffice
      to select only one HTMLSelectElement which will be updated to hold all
      municipalities associated with given federative unit.
    * select_element_in_same_form_as_event: Boolean indicating if the
      municipality select element is the same form as event.target.form.
    * event: Event fired from a HTMLSelectElement holding selected federative
      unit.
  */

  if (
      ! /\s+(?:Number|String)\]$/.test(
        Object.prototype.toString.call(municipality_select_element_string)
      ) ||
      (
        /\s+(?:Number|String)\]$/.test(
          Object.prototype.toString.call(municipality_select_element_string)
        ) &&
        ! municipality_select_element_string.length
      )
    ) {

    throw new TypeError(browser.i18n.getMessage(
      "typeerror_type_and_subtype_expected",
      [
        "municipality_select_element_string",
        "Number, String",
      ]
    ));
  }

  if (select_element_in_same_form_as_event) {
    var municipality_select_element = event.target.form.querySelector(
      municipality_select_element_string
    );
  } else {
    var municipality_select_element = document.querySelector(
      municipality_select_element_string
    );
  }

  if (! (municipality_select_element instanceof HTMLSelectElement)) {
    throw new TypeError(browser.i18n.getMessage(
      "typeerror_instanceof_expected",
      [
        "municipality_select_element",
        "HTMLSelectElement",
      ]
    ));
  } else if (! (event.target instanceof HTMLSelectElement)) {
    throw new TypeError(browser.i18n.getMessage(
      "typeerror_instanceof_expected",
      [
        "event.target",
        "HTMLSelectElement",
      ]
    ));
  }

  if (event.target.value != "") {
    var request = new Request(document.location.origin +
      "/ajax-municipios/" +
      event.target.value
    );

    var response = await fetch(request);

    if (response.status == 200) {
      response = await response.json();

      municipality_property_names = {
        identity: [/^id(?:entidade|entificacao)?[\W_]*municipio$/i],
        real_name: [/^(?:tx|texto)[\W_]*nome[\W_]*municipio$/i],
      }

      var municipalities = new Map(
        [
          [
            "root",
            document.createDocumentFragment(),
          ],
          [
            "list",
            new Map(
              [
                [
                  "root",
                  document.createElement("select"),
                ],
              ]
            ),
          ],
        ]
      );

      municipalities.get("list").get("root").name =
        municipality_select_element.name;
      municipalities.get("list").get("root").id =
        municipality_select_element.id;

      for (let municipality_index in response) {
        if (! response.hasOwnProperty(municipality_index)) {
          continue;
        }

        var municipality = response[municipality_index];

        get_common_property_names_from_member_object(
          municipality_property_names,
          municipality
        );

        municipalities.get("list").set(
          municipality_index,
          document.createElement("option")
        );
        municipalities.get("list").get(municipality_index).value =
          municipality[municipality_property_names.identity[1]];
        municipalities.get("list").get(municipality_index).innerText =
          municipality[municipality_property_names.real_name[1]];
      }

      append_map_entries_to_root_node(municipalities);
      municipality_select_element.parentNode.replaceChild(
        municipalities.get("root"),
        municipality_select_element
      );
    } else {
      console.debug(
        request,
        response,
        await response.text()
      );
      throw new Error(browser.i18n.getMessage(
        "error_unhandled_response_status"
      ));
    }
  }
}

if (/^\/secretaria\/inscricao\/\d+$/i.test(document.location.pathname)) {
  var old_subscription_form = document.forms["form_inscricao"]
  var new_subscription_form =
    clone_element_remvoing_event_handlers_and_listeners(
      old_subscription_form,
      true
  );

  new_subscription_form.querySelector('#uf_pessoal').addEventListener(
    "change",
    update_municipalities_from_federative_unit_select_element.bind(
      null,
      '#id_municipio_endereco_residencial',
      true
    )
  );

  old_subscription_form.parentNode.replaceChild(
    new_subscription_form,
    old_subscription_form
  );
} else if (/^\/aluno\/certificados$/i.test(document.location.pathname)) {
  document.querySelectorAll("button").forEach(function replace_anchor(
      unstandardized_anchor
    ) {

    var matching_attributes_values =
      get_matching_strings_from_selected_elements_attributes(
        unstandardized_anchor_regexp_array[0],
        unstandardized_anchor,
        [
          "onclick",
          [
            "data-librejs-blocked-event",
            undefined,
            "value",
          ],
        ]
    );

    if (
        Object.prototype.toString.call(matching_attributes_values)
          == "[object Map]" &&
        [
          ...matching_attributes_values.keys(),
        ].length
      ) {

      function replace_unstandardized_anchor(value) {
        if ((value instanceof Object)) {
          throw new TypeError(browser.i18n.getMessage(
            "typeerror_instanceof_unexpected",
            [
              value,
              "Object",
            ]
          ));
        }

        var anchor = new Map(
          [
            [
              "root",
              document.createDocumentFragment(),
            ],
            [
              "anchor",
              document.createElement("a"),
            ],
          ]
        );

        anchor.get("anchor").href = document.location.origin +
          "/encerrarInscricao/" +
          value.match(new RegExp(
            unstandardized_anchor_regexp_array.map(function get_regexp(part) {
              return part.source;
            }).join(""),
            "i"
          ))[1];
        anchor.get("anchor").innerText = unstandardized_anchor.innerText;

        append_map_entries_to_root_node(anchor);
        unstandardized_anchor.parentNode.replaceChild(
          anchor.get("root"),
          unstandardized_anchor
        );
      }

      attribute_loop: for (let attribute of
        matching_attributes_values.entries()) {

        if (Object.prototype.toString.call(attribute[1]) == "[object Map]") {
          for (let subattribute of attribute[1].entries()) {
            replace_unstandardized_anchor(subattribute[1]);
            break attribute_loop;
          }
        } else {
          replace_unstandardized_anchor(attribute[1]);
          break attribute_loop;
        }
      }
    }
  });
}
