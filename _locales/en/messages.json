{
  "copyright_and_license_notice": {
    "message": "ChuspaLMS: Client hybridization used as substitute for the proprietary to access learning management systems.\nCopyright (C) 2019-2021, 2024  Adonay Felipe Nogueira <https://libreplanet.org/wiki/User:Adfeno> <adfeno.7046@gmail.com>\n\nThis file is part of ChuspaLMS.\n\nChuspaLMS is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.\n\nChuspaLMS is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.\n\nYou should have received a copy of the GNU General Public License along with ChuspaLMS.  If not, see <https://www.gnu.org/licenses/>."
  },
  "extension_name": {
    "message": "ChuspaLMS",
    "description": "Name of the extension."
  },
  "extension_description": {
    "message": "Client hybridization used as substitute for the proprietary to access learning management systems.",
    "description": "Description of the extension."
  },
  "attempts_left": {
    "message": "Attempts left: $ATTEMPTS$",
    "description": "Displays the remaining attempts for the activity.",
    "placeholders": {
      "attempts": {
        "content": "$1",
        "example": "4"
      }
    }
  },
  "discipline_chat": {
    "message": "Chat"
  },
  "discipline_material": {
    "message": "Material"
  },
  "discipline_forum": {
    "message": "Forum"
  },
  "done": {
    "message": "Done"
  },
  "download": {
    "message": "Download"
  },
  "error_unhandled_response_status": {
    "message": "ChuspaLMS received an unexpected HTTP response.\nThe request or response were printed in your browser's debug output for this webpage so that you can adapt your copy of ChuspaLMS according to the server response or contact the developers of ChuspaLMS.",
    "description": "When a given Fetch API Response has an HTTP status code or condition that weren't expected."
  },
  "form_submission_succeeded": {
    "message": "The form was submitted successfully."
  },
  "grade": {
    "message": "Grade: $GRADE$/$WEIGHT$",
    "description": "Displays the achieved grade over the maximum weight of the activity.",
    "placeholders": {
      "grade": {
        "content": "$1",
        "example": "4.5"
      },
      "weight": {
        "content": "$2",
        "example": "7"
      }
    }
  },
  "last_file_sent" :{
    "message": "Last file sent"
  },
  "last_text_sent" :{
    "message": "Last text sent"
  },
  "no_content": {
    "message": "No content available."
  },
  "reviewers_message": {
    "message": "Reviewer's message"
  },
  "seen": {
    "message": "Seen"
  },
  "send_work": {
    "message": "Send work"
  },
  "start_test": {
    "message": "Start test"
  },
  "submit": {
    "message": "Submit",
    "description": "Displayed for a button to submit a form."
  },
  "syntaxerror_invalid_date_or_time_string": {
    "message": "The $NAME$ variable of value \"$VALUE$\" describes a malformed date or time. The expected format for dates is \"31/12/1998\" and either \"23\", \"23:59\", \"23:59:59\", \"23:59:59.123\" or \"23:59:59,123\" for time.",
    "placeholders": {
      "name": {
        "content": "$1"
      },
      "value": {
        "content": "$2"
      }
    }
  },
  "syntaxerror_invalid_selectors_string": {
    "message": "The $NAME$ variable of value \"$VALUE$\" describes malformed selectors. The expected format is similar to \"level_1[level_2][level_3]\"",
    "placeholders": {
      "name": {
        "content": "$1"
      },
      "value": {
        "content": "$2"
      }
    }
  },
  "typeerror_input_form_type_expected": {
    "message": "Input element represented by $NAME$ variable is of $ORIGINAL$ type, but must be at least one of the following types: $EXPECTED$.",
    "placeholders": {
      "name": {
        "content": "$1"
      },
      "original": {
        "content": "$2",
        "example": "tel"
      },
      "expected": {
        "content": "$3",
        "example": "text, date, tel"
      }
    }
  },
  "typeerror_instanceof_expected": {
    "message": "The $NAME$ variable must be a non-empty instance of at least one of the following constructors: $INSTANCEOF$.",
    "placeholders": {
      "name": {
        "content": "$1"
      },
      "instanceof": {
        "content": "$2",
        "example": "Array"
      }
    }
  },
  "typeerror_instanceof_unexpected": {
    "message": "The $NAME$ variable mustn't be an instance of the following constructors: $INSTANCEOF$.",
    "placeholders": {
      "name": {
        "content": "$1"
      },
      "instanceof": {
        "content": "$2",
        "example": "Array"
      }
    }
  },
  "typeerror_node_has_parent": {
    "message": "Node represented by $NAME$ variable already has a parent.",
    "placeholders": {
      "name": {
        "content": "$1"
      }
    }
  },
  "typeerror_type_and_subtype_expected": {
    "message": "The $NAME$ variable (type and subtype: $ORIGINAL$) mustn't be empty and, furthermore, at least one of the following pairs of type and subtype was expected: $EXPECTED$.",
    "placeholders": {
      "name": {
        "content": "$1"
      },
      "original": {
        "content": "$2",
        "example": "[object Array]"
      },
      "expected": {
        "content": "$3",
        "example": "[object Date]"
      }
    }
  },
  "typeerror_type_and_subtype_unexpected": {
    "message": "The $NAME$ variable has a forbidden type and subtype: $ORIGINAL$.",
    "placeholders": {
      "name": {
        "content": "$1"
      },
      "original": {
        "content": "$2",
        "example": "[object Array]"
      }
    }
  },
  "typeerror_undefined_variable": {
    "message": "Undefined $NAME$ variable.",
    "placeholders": {
      "name": {
        "content": "$1"
      }
    }
  },
  "view": {
    "message": "View"
  },
  "warn_ignored_arguments": {
    "message": "The $NAME$ variable has an excessive number of arguments ($ORIGINAL$) when compared to the expected ($EXPECTED$).",
    "placeholders": {
      "name": {
        "content": "$1"
      },
      "original": {
        "content": "$2"
      },
      "expected": {
        "content": "$3"
      }
    }
  }
}
